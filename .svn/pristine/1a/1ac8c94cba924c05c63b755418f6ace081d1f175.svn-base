package com.yqwl.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.dao.LoginLogMapper;
import com.yqwl.dao.SubbranchMapper;
import com.yqwl.pojo.Deliveryman;
import com.yqwl.pojo.LoginLog;
import com.yqwl.pojo.Subbranch;
import com.yqwl.service.DeliverymanService;

@Service("deliverymanService")
@Transactional(rollbackFor = { Exception.class })
public class DeliverymanServiceImpl implements DeliverymanService {
	@Resource
	private DeliverymanMapper deliverymanMapper;
	@Resource
	private SubbranchMapper subbranchMapper;
	@Resource
	private LoginLogMapper loginLogMapper;

	@Override
	public Map<String, Object> listAll(Integer page, Integer limit, String phone) throws Exception {
		Integer resultCount = deliverymanMapper.listAllCount(phone);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Deliveryman> result = deliverymanMapper.listAll(beginPageIndex, limit, phone);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public Deliveryman getById(Long id) {
		return deliverymanMapper.selectByPrimaryKey(id);
	}

	@Override
	public int insert(Deliveryman deliveryman) throws Exception {
		return deliverymanMapper.insertSelective(deliveryman);
	}

	@Override
	public int update(Deliveryman deliveryman) throws Exception {
		return deliverymanMapper.updateByPrimaryKeySelective(deliveryman);
	}

	@Override
	public int dalete(Long id) throws Exception {
		return deliverymanMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Deliveryman getByDeliveryman(String phone, Long subId, String ip) throws Exception {
		Subbranch subbranch = subbranchMapper.selectByPrimaryKey(subId);
		Deliveryman deliveryman = deliverymanMapper.getByDeliveryman(phone, subId);
		if (deliveryman != null) {
			LoginLog log = new LoginLog();
			log.setIp(ip);
			log.setSubbranch_name(subbranch.getName());
			log.setDate(new Date());
			log.setId(deliveryman.getId());
			loginLogMapper.insertSelective(log);
		}
		return deliveryman;
	}

	@Override
	public List<Deliveryman> listBySubId(Long subId) throws Exception {
		List<Deliveryman> list = new LinkedList<>();
		list = deliverymanMapper.listBySubId(subId);
		if (list.size()==0) {
			list.add(new Deliveryman());
			return list;
		}
		return list;
	}

}
