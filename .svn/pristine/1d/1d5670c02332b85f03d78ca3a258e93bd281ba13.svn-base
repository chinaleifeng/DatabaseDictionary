package com.yqwl.controller.appFront;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.GoodsType;
import com.yqwl.service.GoodsService;
import com.yqwl.service.GoodsTypeService;
import com.yqwl.vo.GoodsVo;

/**
 * 
 * @ClassName: GoodsFrontControoller
 * @description 用户端商品控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:32:06
 */
@Controller
@RequestMapping("goodsFront")
@Scope("prototype")
public class GoodsFrontControoller {

	@Resource
	private GoodsService goodsService;
	@Resource
	private GoodsTypeService goodsTypeService;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * @Title: getById
	 * @description 通过ID查询商品信息
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:47:57
	 */
	@ResponseBody
	@RequestMapping(value = "getById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			GoodsVo goods = goodsService.getById(id);
			if (goods != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, goods);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: findHot
	 * @description: 查询上架商品 《没用了》
	 *
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年1月15日-下午4:31:12
	 */
	@ResponseBody
	@RequestMapping(value = "findHot", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findHot(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			List<GoodsVo> goods = goodsService.findHot();
			if (goods != null) {
				code = 0;
				msg = "查询商品成功";
				return FastJsonUtil.getResponseJson(code, msg, goods);
			}
			code = 1;
			msg = "查询商品失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listByType
	 * @description 根据商品类别查询商品
	 * @param type
	 * @param limit
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:00:31
	 */
	@ResponseBody
	@RequestMapping(value = "listAllByType", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listByType(Integer type, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			List<GoodsVo> list = goodsService.listByType(type, limit);
			if (list != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, list);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	@ResponseBody
	@RequestMapping(value = "listGoodsByName", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listGoodsByName(@RequestParam(value = "name", required = false) String name, Integer limit,
			HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			List<GoodsVo> list = goodsService.listGoodsByName(name, limit);
			if (list != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, list);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listAll
	 * @description 商城首页显示分类
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年3月18日 下午4:00:19
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(HttpSession session,Long pid) {
		int code = 0;
		String msg = null;
		try {
			List<GoodsType> GoodsType = goodsTypeService.listAllByPid(pid);
			if (GoodsType != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, GoodsType);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
