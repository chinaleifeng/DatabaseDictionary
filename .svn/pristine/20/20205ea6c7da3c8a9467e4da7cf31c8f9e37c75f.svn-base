package com.yqwl.controller.appBack;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.common.utils.GetLocationBaiduMap;
import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.pay.utils.IpUtils;
import com.yqwl.pojo.Deliveryman;
import com.yqwl.pojo.Subbranch;
import com.yqwl.service.DeliverymanService;
import com.yqwl.service.SubbranchService;

/**
 * 
 * @ClassName: AppBackLoginController
 * @description 小程序分店登陆控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:30:52
 */
@Controller
@RequestMapping("appBackLogin")
@Scope("prototype")
public class AppBackLoginController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private SubbranchService subbranchService;
	@Resource
	private DeliverymanService deliverymanService;
	@Resource
	private DeliverymanMapper deliverymanMapper;
	/**
	 * 
	 * @Title: login
	 * @description 分店登陆
	 * @param account
	 * @param password
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:33:41
	 */
	@ResponseBody
	@RequestMapping(value = "login", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String login(@RequestParam(value = "account", required = true) String account,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "phone", required = true) String phone,
			
			@RequestParam(value = "ip", required = true) String ip, HttpServletRequest request,
			HttpSession session) {
		int code = 0;
		String msg = null;
		try {
		//	String ip = GetLocationBaiduMap.getLocationByBaiduMap(longitude,latitude);
		
			Subbranch boss = subbranchService.login(account, password);
			if (boss == null) {
				code = 1;
				msg = "账号或密码错误";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
			Deliveryman deliveryman = deliverymanService.getByDeliveryman(phone, boss.getId(), ip);
			Long deliverymanId =deliverymanMapper.getByPhone(phone).getId();
			if (deliveryman == null) {
				code = 1;
				msg = "手机号错误或本店没有该配送员";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
			session.setAttribute("boss", boss);
			session.setAttribute("deliverymanId", deliverymanId);
		//	System.out.println("session:"+session);
			code = 0;
			msg = "登陆成功";
			return FastJsonUtil.getResponseJson(code, msg, boss);
		} catch (Exception e) {
			code = -1;
			msg = "登陆失败";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

}
