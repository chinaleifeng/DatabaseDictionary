package com.yqwl.controller.appFront;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.DeliveryAddress;
import com.yqwl.pojo.User;
import com.yqwl.pojo.UserDiscounts;
import com.yqwl.service.DeliveryAddressService;
import com.yqwl.service.UserDiscountsService;
import com.yqwl.service.UserService;

/**
 * 
 * @ClassName: MineController
 * @description 用户端我的控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:32:20
 */
@Controller
@RequestMapping("mine")
@Scope("prototype")
public class MineController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Resource
	private UserDiscountsService userDiscountsService;
	@Resource
	private DeliveryAddressService deliveryAddressService;
	@Resource
	private UserService userService;
	/**
	 * 
	 * @Title: selectById
	 * @description: 根据id查询用户信息
	 *
	 * @param id
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月28日-下午2:55:09
	 */
	@ResponseBody
	@RequestMapping(value = "selectById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String selectById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
		
		
				User users = userService.selectById(id);
				if (users != null) {
					code = 0;
					msg = "查询用户成功";
					return FastJsonUtil.getResponseJson(code, msg, users);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: getDiscountsByUserId
	 * @description 查询用户优惠券
	 * @param userId
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月16日 上午9:10:36
	 */
	@ResponseBody
	@RequestMapping(value = "getDiscountsByUserId", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getDiscountsByUserId(Long userId, Integer status) {
		int code = 0;
		String msg = null;
		try {
			
			if (userId != null) {
				List<UserDiscounts> userDiscounts = userDiscountsService.getByUserId(userId, status);
				if (userDiscounts != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, userDiscounts);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: insertDeliveryAddress
	 * @description 新增收货地址
	 * @param deliveryAddress
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:01:08
	 */
	@ResponseBody
	@RequestMapping(value = "insertDeliveryAddress", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String insertDeliveryAddress(DeliveryAddress deliveryAddress, HttpSession session) {
		int code = 0;
		
		String msg = null;
		try {
		
			if (deliveryAddress.getUser_id() != null) {
				int i = deliveryAddressService.insertDeliveryAddress(deliveryAddress);
				if (i != 0) {
					code = 0;
					msg = "增加成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "增加失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: deleteDeliveryAddress
	 * @description 删除收货地址
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:01:44
	 */
	@ResponseBody
	@RequestMapping(value = "deleteDeliveryAddress", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String deleteDeliveryAddress(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			
		
				int i = deliveryAddressService.deleteDeliveryAddress(id);
				if (i != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listtDeliveryAddressByUserId
	 * @description 根据用户查查询收货地址
	 * @param userId
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:01:55
	 */
	@ResponseBody
	@RequestMapping(value = "listtDeliveryAddressByUserId", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listtDeliveryAddressByUserId(Long userId, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
		
			if (userId != null) {
				List<DeliveryAddress> result = deliveryAddressService.listtDeliveryAddressByUserId(userId);
				if (result != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, result);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: updateDeliveryAddress
	 * @description 修改收货地址
	 * @param deliveryAddress
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:02:11
	 */
	@ResponseBody
	@RequestMapping(value = "updateDeliveryAddress", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String updateDeliveryAddress(DeliveryAddress deliveryAddress, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
		
			if (deliveryAddress.getUser_id() != null) {
				int i = deliveryAddressService.updateDeliveryAddress(deliveryAddress);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: getDeliveryAddressById
	 * @description 查询单条收货地址
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午3:02:23
	 */
	@ResponseBody
	@RequestMapping(value = "getDeliveryAddressById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getDeliveryAddressById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
		
		
				DeliveryAddress deliveryAddress = deliveryAddressService.getDeliveryAddressById(id);
				if (deliveryAddress != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, deliveryAddress);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
