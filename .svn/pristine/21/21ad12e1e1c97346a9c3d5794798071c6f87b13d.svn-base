package com.yqwl.controller.appFront;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.BusinessHours;
import com.yqwl.service.BusinessHoursService;

@Controller
@RequestMapping("frontBusinessHours")
@Scope("prototype")
/**
 * 
 *
 * @ClassName: FrontBusinessHoursController
 *
 * @description 小程序营业时间
 *
 * @author HanMeng
 * @createDate 2019年5月24日-上午10:59:10
 */
public class FrontBusinessHoursController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Resource
	private BusinessHoursService businessHoursService;

	/**
	 * 
	 * @Title: update
	 * @description: 修改营业时间
	 *
	 * @param businessHours
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年5月24日-上午10:58:07
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String update(BusinessHours businessHours, HttpSession session) {
		int code = 0;
		String msg = null;
		try {

			int i = businessHoursService.update(businessHours);
			if (i != 0) {
				code = 0;
				msg = "修改成功";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: findById
	 * @description: 查找营业时间记录，只有一条所以不要参数
	 *
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年5月20日-下午2:37:48
	 */
	@ResponseBody
	@RequestMapping(value = "findById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findById(HttpSession session) {

		int code = 0;
		String msg = null;
		try {

			BusinessHours businessHours = businessHoursService.findById(1L);
			if (businessHours != null) {
				code = 0;
				msg = "本店营业时间:"+businessHours.getStart_time()+":00-"+businessHours.getStart_time()+":00";
				return FastJsonUtil.getResponseJson(code, msg, businessHours);

			} else {
				code = 2;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
