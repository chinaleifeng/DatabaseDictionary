package com.yqwl.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yqwl.common.utils.DateUtil;
import com.yqwl.common.utils.JYSMSUtil;
import com.yqwl.dao.DeliveryAddressMapper;
import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.dao.GeneralizeMapper;
import com.yqwl.dao.GoodsImagesMapper;
import com.yqwl.dao.GoodsMapper;
import com.yqwl.dao.OrderDetailsMapper;
import com.yqwl.dao.OrderMapper;
import com.yqwl.dao.ShoppingTrolleyMapper;
import com.yqwl.dao.SubbranchMapper;
import com.yqwl.dao.UserDiscountsMapper;
import com.yqwl.dao.UserMapper;
import com.yqwl.pojo.DeliveryAddress;
import com.yqwl.pojo.Deliveryman;
import com.yqwl.pojo.Generalize;
import com.yqwl.pojo.Goods;
import com.yqwl.pojo.GoodsImages;
import com.yqwl.pojo.Order;
import com.yqwl.pojo.OrderDetails;
import com.yqwl.pojo.ShoppingTrolley;
import com.yqwl.pojo.User;
import com.yqwl.pojo.UserDiscounts;
import com.yqwl.service.OrderService;
import com.yqwl.service.RepertoryService;
import com.yqwl.vo.Achievement;
import com.yqwl.vo.OrderVo;

@Service("orderService")
@Transactional(rollbackFor = { Exception.class })
public class OrderServiceImpl implements OrderService {

	@Resource
	private OrderMapper orderMapper;
	@Resource
	private OrderDetailsMapper orderDetailsMapper;
	@Resource
	private GoodsMapper goodsMapper;
	@Resource
	private GoodsImagesMapper goodsImagesMapper;
	@Resource
	private ShoppingTrolleyMapper shoppingTrolleyMapper;
	@Resource
	private DeliveryAddressMapper DeliveryAddressMapper;
	@Resource
	private UserDiscountsMapper userDiscountsMapper;
	@Resource
	private SubbranchMapper subbranchMapper;
	@Resource
	private GeneralizeMapper generalizeMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private RepertoryService repertoryService;
	@Resource
	private DeliverymanMapper deliverymanMapper;
	@Override
	public List<OrderVo> listByStatus(Integer limit, Integer status, Integer subbranchId,String phone) throws Exception {
		Deliveryman deliveryman = deliverymanMapper.getByPhone(phone);
		// 查询所有订单
		List<Order> orders = orderMapper.listByStatus(limit, status, subbranchId,deliveryman.getId());
		List<OrderVo> orderVos = new LinkedList<>();
		for (Order order : orders) {
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			// 根据订单查询订单详情
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
			int i = 0;
			for (OrderDetails orderDetails : list) {
				i += orderDetails.getGoods_count();
			}
			DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(order.getAddress_id());
			if(deliveryAddress != null){
				orderVo.setLatitude(deliveryAddress.getLatitude());
				orderVo.setLongitude(deliveryAddress.getLongitude());
			}
			orderVo.setCount(i);
			orderVo.setOrderDetails(list);
			orderVos.add(orderVo);
		}
		return orderVos;
	}

	@Override
	public Achievement Achievements(Integer subbranchId, String time) throws Exception {
		long lt = new Long(time);
		Date date = new Date(lt);
		Achievement achievement = orderMapper.Achievements(subbranchId, DateUtil.getYearMonth(date));
		if (achievement.getTurnover() == null) {
			achievement.setTurnover(0);
		}
		return achievement;
	}

	@Override
	public int transferOrder(Long id, Long subbranchId) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		order.setSubbranch_id(subbranchId);
		return orderMapper.updateByPrimaryKeySelective(order);
	}

	@Override
	public int updateToStatus(Long id, Integer status) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		order.setStatus(status);
		// 订单状态为2（正在配送）时发送短信
		if (status.equals(2)) {
			Deliveryman deliveryman = deliverymanMapper.selectByPrimaryKey(order.getDeliveryman_id());
			Map<String, String> map = new HashMap<>();
			// 订单号
			map.put("id", order.getId().toString());
			// 配送员姓名
			map.put("name", deliveryman.getName());
			// 配送员电话
			map.put("phone", deliveryman.getPhone());
			JYSMSUtil.sendMessage(order.getPhone(), "3515", map);
			// 订单状态等于3（已送达）
		} else if (status.equals(3)) {
			Long userId = order.getUser_id();
			Generalize generalize = generalizeMapper.getby_promoter(userId);
			// 判断该用户是否有推广人
			if (generalize != null) {
				// 查询出商品详情
				List<OrderDetails> listRecord = orderDetailsMapper.listByOrderId(order.getId());
				BigDecimal count = new BigDecimal(0.00);
				for (OrderDetails orderDetails : listRecord) {
					Goods goods = goodsMapper.selectByPrimaryKey(orderDetails.getGoods_id());
					// 计算出每个商品的推广金总额
					count = count.add(goods.getGeneralize_money().multiply(new BigDecimal(orderDetails.getGoods_count())));
				}
				// 给推广人增加推广金
				User user = userMapper.selectByPrimaryKey(generalize.getPromoter());
				user.setGeneralize_money(user.getGeneralize_money().add(count));
				userMapper.updateByPrimaryKeySelective(user);
			}
		}
		return orderMapper.updateByPrimaryKeySelective(order);
	}

	@Override
	public int insert(Order order, Long address_id, Long[] ids) throws Exception {
		if (order.getRemarks().equals("null")) {
			order.setRemarks(null);
		}
		// 时间戳+随机数拼成订单号
		Long time = System.currentTimeMillis();
		Random rand = new Random();
		String num = String.valueOf((rand.nextInt(899) + 100));
		Long OrderId = Long.valueOf((time + num));
		// 查询收获地址
		DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(address_id);
		order.setId(OrderId);
		order.setAddress_id(address_id);
		
		order.setAddress(deliveryAddress.getLocation() + deliveryAddress.getAddress());
		order.setName(deliveryAddress.getName());
		order.setPhone(deliveryAddress.getPhone());
		order.setStatus(0);
		order.setTime(new Date());
		order.setType(order.getType());
		// 遍历购物车ID
		for (Long id : ids) {
			OrderDetails orderDetails = new OrderDetails();
			// 查询购物车信息
			ShoppingTrolley shoppingTrolley = shoppingTrolleyMapper.selectByPrimaryKey(id);
			Goods goods = goodsMapper.selectByPrimaryKey(shoppingTrolley.getGoods_id());
			// 查询商品图标类型等于3（缩略图）的图片
			List<GoodsImages> list = goodsImagesMapper.getByGoodsIdAndType3(goods.getId());
			for (GoodsImages goodsImages : list) {
				orderDetails.setGoods_images(goodsImages.getUrl());
			}
			orderDetails.setOrder_id(OrderId);
			orderDetails.setGoods_id(goods.getId());
			orderDetails.setGoods_name(goods.getName());
			orderDetails.setGoods_count(shoppingTrolley.getConut());
			orderDetails.setGoods_original_money(goods.getOriginal_money());
			orderDetails.setGoods_describe(goods.getDescribe());
			orderDetails.setGoods_money(goods.getMoney());
			// 判断冰镇数量
			if (shoppingTrolley.getLced() != 0) {
				// 判断时候有备注
				if (order.getRemarks() == null || order.getRemarks().equals("")) {
					order.setRemarks("冰镇" + goods.getName() + shoppingTrolley.getLced() + "瓶");
				} else {
					order.setRemarks(order.getRemarks() + ",冰镇" + goods.getName() + shoppingTrolley.getLced() + "瓶");
				}
			}
			orderDetailsMapper.insertSelective(orderDetails);
			shoppingTrolleyMapper.deleteByPrimaryKey(id);
		}
		// 判断是否使用优惠券
		if (order.getUser_discounts_id() != 0) {
			UserDiscounts record = new UserDiscounts();
			record.setId(order.getUser_discounts_id());
			record.setStatus(1);
			userDiscountsMapper.updateByPrimaryKeySelective(record);
		}
		int i = orderMapper.insertRecord(order);
		return i;
	}

	/**
	 * 
	 * @Title: findNoDistribution
	 * @description: 查询订单状态（0 未支付 1、未配送，2、配送中，3、已送达，4、退货）
	 *
	 * @param userId
	 * @return
	 * @return List<Order>
	 *
	 * @author HanMeng
	 * @createDate 2019年1月15日-下午5:12:56
	 */

	@Override
	public List<OrderVo> findOrder(Integer userId, Integer status, Integer limit) throws Exception {
		List<OrderVo> orders = orderMapper.findOrder(userId, status, limit);
		List<OrderVo> orderVos = new LinkedList<>();
		for (Order order : orders) {
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
			
			int i = 0;
			for (OrderDetails orderDetails : list) {
			
				i = i + orderDetails.getGoods_count();
			}
			orderVo.setCount(i);
			orderVo.setOrderDetails(list);
			orderVos.add(orderVo);
		}

		return orderVos;
		

	}

	@Override
	public int insertOrder(Order order, Long address_id, Long id, Integer count, Integer Lced) throws Exception {
		if (order.getRemarks().equals("null")) {
			order.setRemarks(null);
		}
		// 时间戳+随机数拼成订单号
		Long time = System.currentTimeMillis();
		Random rand = new Random();
		String num = String.valueOf((rand.nextInt(899) + 100));
		Long OrderId = Long.valueOf((time + num));

		DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(address_id);
		order.setId(OrderId);
		order.setAddress_id(address_id);
		order.setAddress(deliveryAddress.getLocation() + deliveryAddress.getAddress());
		order.setName(deliveryAddress.getName());
		order.setPhone(deliveryAddress.getPhone());
		order.setStatus(0);
		order.setTime(new Date());
		order.setType(order.getType());

		OrderDetails orderDetails = new OrderDetails();

		Goods goods = goodsMapper.selectByPrimaryKey(id);
		List<GoodsImages> list = goodsImagesMapper.getByGoodsIdAndType3(goods.getId());
		for (GoodsImages goodsImages : list) {
			orderDetails.setGoods_images(goodsImages.getUrl());
		}
		orderDetails.setOrder_id(OrderId);
		orderDetails.setGoods_id(goods.getId());
		orderDetails.setGoods_name(goods.getName());
		orderDetails.setGoods_count(count);
		orderDetails.setGoods_original_money(goods.getOriginal_money());
		orderDetails.setGoods_describe(goods.getDescribe());
		orderDetails.setGoods_money(goods.getMoney());
		if (Lced != 0) {
			if (order.getRemarks() == null) {
				order.setRemarks("冰镇" + goods.getName() + Lced + "瓶");
			} else {
				order.setRemarks(order.getRemarks() + ",冰镇" + goods.getName() + Lced + "瓶");
			}
		}
		orderDetailsMapper.insertSelective(orderDetails);
		if (order.getUser_discounts_id() != 0) {
			UserDiscounts record = new UserDiscounts();
			record.setId(order.getUser_discounts_id());
			record.setStatus(1);
			userDiscountsMapper.updateByPrimaryKeySelective(record);
		}
		int i = orderMapper.insertRecord(order);
		return i;
	}

	@Override
	public int deleteOrder(Long id) throws Exception {
		int i = 0;
		Order order = orderMapper.selectByPrimaryKey(id);
		// 判断订单状态等于0和3的时候可以删除订单
		if (order.getStatus() == 0 || order.getStatus() == 3) {
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(id);

			// 循环删除订单详情
			for (OrderDetails orderDetails : list) {
				orderDetailsMapper.deleteByPrimaryKey(orderDetails.getId());
			}
			i = orderMapper.deleteByPrimaryKey(id);
		} else {
			i = -1;
		}
		return i;
	}

	/**
	 * 
	 * 
	 * @Title: OrderServiceImpl.java
	 * 
	 * @Package com.yqwl.service.impl
	 * 
	 * @Description: 修改订单状态
	 * 
	 * @author HanMeng
	 * 
	 * @date 2019年2月12日-上午10:23:57
	 */
	@Override
	public int updateToOrderStatus(Long id, Integer status) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
	
		order.setStatus(status);
		return orderMapper.updateByPrimaryKeySelective(order);

	}

	@Override
	public Map<String, Object> listAll(String hm,Integer page, Integer limit) throws Exception {
		Integer resultCount = orderMapper.listAllCount(hm);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Order> result = orderMapper.listAll(hm,beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public OrderVo getByid(Long id) throws Exception {
		OrderVo orderVo = new OrderVo();
		Order order  = orderMapper.selectByPrimaryKey(id);
		BeanUtils.copyProperties(order, orderVo);
		orderVo.setOrderDetails(orderDetailsMapper.listByOrderId(id));
		return orderVo;
	}

	@Override
	public Map<String, Object> getByStatus(Integer status,Integer page, Integer limit) throws Exception {
		Integer resultCount = orderMapper.getByStatusCount(status);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Order> result = orderMapper.getByStatus(status,beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public int updateByDeliveryman(Order order) throws Exception {
		return orderMapper.updateByPrimaryKeySelective(order);
	}

	@Override
	public int rob(Long id, String phone) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		if (order.getDeliveryman_id() == null) {
			Deliveryman deliveryman = deliverymanMapper.getByPhone(phone);
			order.setDeliveryman_id(deliveryman.getId());
			order.setDeliveryman_name(deliveryman.getName());
			return orderMapper.updateByPrimaryKeySelective(order);
		}
		return -3;
	}

	@Override
	public List<OrderVo> robOrder(Integer limit, Integer subbranchId) throws Exception {
		// 查询所有订单
		List<Order> orders = orderMapper.robOrder(limit,subbranchId);
		List<OrderVo> orderVos = new LinkedList<>();
		for (Order order : orders) {
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			// 根据订单查询订单详情
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
			int i = 0;
			for (OrderDetails orderDetails : list) {
				i += orderDetails.getGoods_count();
			}
			DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(order.getAddress_id());
			if (deliveryAddress != null) {
				orderVo.setLatitude(deliveryAddress.getLatitude());
				orderVo.setLongitude(deliveryAddress.getLongitude());
			}
		
			orderVo.setCount(i);
			orderVo.setOrderDetails(list);
			orderVos.add(orderVo);
		}
		return orderVos;
	}

}
