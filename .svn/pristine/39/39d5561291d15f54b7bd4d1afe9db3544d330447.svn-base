package com.yqwl.controller.appFront;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vdurmont.emoji.EmojiParser;
import com.yqwl.common.utils.AesCbcUtil;
import com.yqwl.common.utils.DesUtil;
import com.yqwl.common.utils.HttpRequest;
import com.yqwl.dao.UserDiscountsMapper;
import com.yqwl.pay.WxPayConfig;
import com.yqwl.pojo.Discounts;
import com.yqwl.pojo.User;
import com.yqwl.pojo.UserDiscounts;
import com.yqwl.service.DiscountsService;
import com.yqwl.service.UserDiscountsService;
import com.yqwl.service.UserService;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/appFrontLogin")
@Scope("prototype")
public class DecodeUserInfo {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private UserService userService;
	@Resource
	private UserDiscountsMapper userDiscountsMapper;
	@Resource
	private UserDiscountsService userDiscountsService;
	@Resource
	private DiscountsService discountsService;

	/**
	 * 
	 * @Title: decodeUserInfo
	 * @description: 解密用户敏感数据
	 *
	 * @param encryptedData
	 *            明文,加密数据
	 * @param iv
	 *            加密算法的初始向量
	 * @param code
	 *            用户允许登录后，回调内容会带上 code（有效期五分钟），开发者需要将 code 发送到开发者服务器后台，使用code 换取
	 *            session_key api，将 code 换成 openid 和 session_key
	 * @return
	 * @return Map
	 *
	 * @author HanMeng
	 * @throws Exception
	 * @createDate 2019年1月11日-下午2:13:08
	 */

	@ResponseBody
	@RequestMapping(value = "/decodeUserInfo", method = RequestMethod.POST)

	public Map<String, Object> decodeUserInfo(String encryptedData, String iv, String code, HttpSession session)
			throws Exception {
		User user = new User();
		Map<String, Object> map = new HashMap<String, Object>();

		// 登录凭证不能为空
		if (code == null || code.length() == 0) {
			map.put("status", 0);
			map.put("msg", "code 不能为空");
			return map;
		}
		// 小程序唯一标识 (在微信小程序管理后台获取)正式
		String wxspAppid = WxPayConfig.appid;
		// 小程序的 app secret (在微信小程序管理后台获取)正式
		String wxspSecret = WxPayConfig.appSecret;
		// 授权（必填）
		String grant_type = "authorization_code";
		// 1、向微信服务器 使用登录凭证 code 获取 session_key 和 openid
		// 请求参数
		String params = "appid=" + wxspAppid + "&secret=" + wxspSecret + "&js_code=" + code + "&grant_type="
				+ grant_type;
		// 发送请求
		String sr = HttpRequest.sendGet("https://api.weixin.qq.com/sns/jscode2session", params);
		// System.out.println("params:" + params);
		// 解析相应内容（转换成json对象）
		JSONObject json = JSONObject.fromObject(sr);
		// System.out.println("json:" + json);
		// 获取会话密钥（session_key）
		String session_key = json.get("session_key").toString();

		// 用户的唯一标识（openid）
		String openid = (String) json.get("openid");

		
		// 根据获取的openid 查询数据库中是否存在这个openId
		try {
			User openId = userService.findByOpenId(openid);
			if (openId == null) {
				// 向数据库存储openId
				user.setOpenId(openid);
				user.setTime(new Date());
				userService.insert(user);
			}
			//////////////// 2、对encryptedData加密数据进行AES解密 ////////////////
			User userID = userService.findByOpenId(openid);
			String result = AesCbcUtil.decrypt(encryptedData, session_key, iv, "UTF8");
			System.out.println("result:"+result);
			if (null != result && result.length() > 0) {
				JSONObject userInfoJSON = JSONObject.fromObject(result);
				// System.out.println("userInfoJSON:" + userInfoJSON);
				Map<String, Object> userInfo = new HashMap<String, Object>();
				userInfo.put("userId", userID.getId());
				userInfo.put("openId", userInfoJSON.get("openId"));
				userInfo.put("nickName", userInfoJSON.get("nickName"));
				
				// wechatInfo.setUserNickName(DesUtil.encode( userInfoJSON.get("nickName"), "utf-8"));//将微信昵称用utf-8编码后储存
				System.out.println("nickName:"+userInfoJSON.get("nickName"));
				userInfo.put("gender", userInfoJSON.get("gender"));
				userInfo.put("city", userInfoJSON.get("city"));
				userInfo.put("province", userInfoJSON.get("province"));
				userInfo.put("country", userInfoJSON.get("country"));
				userInfo.put("avatarUrl", userInfoJSON.get("avatarUrl"));
				/*String regEx="[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]"; 
				Pattern p = Pattern.compile(regEx); 
				Matcher m = p.matcher(userInfoJSON.get("nickName").toString());
				return m.replaceAll("").trim();*/
				
				String wxName = EmojiParser.removeAllEmojis(userInfoJSON.get("nickName").toString());

				user.setWx_name(wxName);
				//user.setWx_name(userInfoJSON.get("nickName").toString());
				userService.update(user);
				// 查询是否有首单优惠券
				List<Discounts> discountsList = discountsService.findByFirstOrder();
				
				System.out.println("有几个首单优惠券 :" + discountsList.size());
				// 不为空则肯定有首单优惠券
				if (discountsList != null) {
					// 首单优惠券
					for (Discounts discounts : discountsList) {
						List<UserDiscounts> i = userDiscountsService.findByUserId(userID.getId(), discounts.getId());
						
						// i为空，说明用户没有获取过此优惠券 可以发送到该用户账号
						if (i.isEmpty()) {
						

							UserDiscounts userDiscounts = new UserDiscounts();
							userDiscounts.setDiscounts_id(discounts.getId());
							userDiscounts.setStart_money(discounts.getStart_money());
							userDiscounts.setSubtract_money(discounts.getSubtract_money());
							userDiscounts.setStatus(0);
							userDiscounts.setUser_id(userID.getId());
							userDiscounts.setTime(new Date());
							int userDis = userDiscountsMapper.insertSelective(userDiscounts);
							//System.out.println("ooooooooooo:" + userDis);
							if (userDis > 0) {
								logger.info("用户" + userID.getId() + "插入首单优惠券成功");
							}
						}

					}

				}

				// 解密unionId & openId;
				// userInfo.put("unionId", userInfoJSON.get("unionId"));
				map.put("userInfo", userInfo);
				// System.out.println("map:" + map);
				map.put("status", 0);
				map.put("msg", "解密成功");
				// System.out.println("userInfo:" + userInfo);
				session.setAttribute("login_user", userID);

			} else {
				map.put("status", -1);
				map.put("msg", "解密失败");
			}
			// System.out.println("我在缓存中：" + userID);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("status", -1);
			map.put("msg", "系统异常");
			return map;
		}

	}

}
