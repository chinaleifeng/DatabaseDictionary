package com.yqwl.controller.appFront;


import java.util.List;

import javax.annotation.Resource;


import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.ShoppingTrolley;
import com.yqwl.pojo.User;
import com.yqwl.service.ShoppingTrolleyService;
import com.yqwl.vo.ShoppingTrolleyVo;
/**
 * 
 * @ClassName: ShoppingTrolleyController
 * @description 用户端购物车控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:32:44
 */
@Controller
@RequestMapping("shoppingTrolley")
@Scope("prototype")
public class ShoppingTrolleyController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	private ShoppingTrolleyService shoppingTrolleyService;
	
	/**
	 * 
	 * @Title: edit
	 * @description 编辑购物车
	 * @param shoppingTrolley
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月14日 下午2:00:46
	 */
	@ResponseBody
	@RequestMapping(value = "edit", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String edit(ShoppingTrolley shoppingTrolley,HttpSession session) {
		int code = 0; 
		String msg = null;
		try {
			User user = (User) session.getAttribute("login_user");
			if (user != null) {
				Long id = shoppingTrolleyService.insert(shoppingTrolley);
				if (id != 0) {
					code = 0;
					msg = "加入购物车成功";
					return FastJsonUtil.getResponseJson(code, msg, id);
				}
				code = 1;
				msg = "加入购物车失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: delete
	 * @description 从购物车删除商品
	 * @param id
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月14日 下午2:02:54
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String delete(Long id,HttpSession session) {
		int code = 0; 
		String msg = null;
		try {
			User user = (User) session.getAttribute("login_user");
			if (user != null) {
				int i = shoppingTrolleyService.delete(id);
				if (i != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	
	/**
	 * 
	 * @Title: listAllByUserId
	 * @description 根据UserId查询购物车
	 * @param UserId
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月15日 上午11:33:14
	 */
	@ResponseBody
	@RequestMapping(value = "listAllByUserId", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAllByUserId(Long UserId,HttpSession session) {
		int code = 0; 
		String msg = null;
		try {
			User user = (User) session.getAttribute("login_user");
			if (user != null) {
				System.out.println(UserId);
				List<ShoppingTrolleyVo> result = shoppingTrolleyService.listAllByUserId(UserId);
				if (result != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, result);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	
}	
