package com.yqwl.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.yqwl.common.utils.DateUtil;
import com.yqwl.common.utils.JYSMSUtil;
import com.yqwl.dao.BonusInfoMapper;
import com.yqwl.dao.DeductionMapper;
import com.yqwl.dao.DeliveryAddressMapper;
import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.dao.DiscountsMapper;
import com.yqwl.dao.GeneralizeMapper;
import com.yqwl.dao.GoodsImagesMapper;
import com.yqwl.dao.GoodsMapper;
import com.yqwl.dao.OrderDetailsMapper;
import com.yqwl.dao.OrderMapper;
import com.yqwl.dao.SettingMapper;
import com.yqwl.dao.ShoppingTrolleyMapper;
import com.yqwl.dao.SubbranchMapper;
import com.yqwl.dao.UserDiscountsMapper;
import com.yqwl.dao.UserMapper;
import com.yqwl.pojo.BonusInfo;
import com.yqwl.pojo.Deduction;
import com.yqwl.pojo.DeliveryAddress;
import com.yqwl.pojo.Deliveryman;
import com.yqwl.pojo.Discounts;
import com.yqwl.pojo.Generalize;
import com.yqwl.pojo.Goods;
import com.yqwl.pojo.GoodsImages;
import com.yqwl.pojo.Order;
import com.yqwl.pojo.OrderDetails;
import com.yqwl.pojo.Setting;
import com.yqwl.pojo.ShoppingTrolley;
import com.yqwl.pojo.User;
import com.yqwl.pojo.UserDiscounts;
import com.yqwl.service.OrderService;
import com.yqwl.service.RepertoryService;
import com.yqwl.vo.Achievement;
import com.yqwl.vo.OrderVo;

@Service("orderService")
@Transactional(rollbackFor = { Exception.class })
public class OrderServiceImpl implements OrderService {

	@Resource
	private OrderMapper orderMapper;
	@Resource
	private OrderDetailsMapper orderDetailsMapper;
	@Resource
	private GoodsMapper goodsMapper;
	@Resource
	private GoodsImagesMapper goodsImagesMapper;
	@Resource
	private ShoppingTrolleyMapper shoppingTrolleyMapper;
	@Resource
	private DeliveryAddressMapper DeliveryAddressMapper;
	@Resource
	private UserDiscountsMapper userDiscountsMapper;
	@Resource
	private SubbranchMapper subbranchMapper;
	@Resource
	private GeneralizeMapper generalizeMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private RepertoryService repertoryService;
	@Resource
	private DeliverymanMapper deliverymanMapper;
	@Resource
	private BonusInfoMapper bonusInfoMapper;
	@Resource
	private DiscountsMapper discountsMapper;
	@Resource
	private DeductionMapper deductionMapper;
	@Resource
	private SettingMapper settingMapper;
	@Override
	public List<OrderVo> listByStatus(Integer limit, Integer status, Integer subbranchId, String phone)
			throws Exception {
		Deliveryman deliveryman = deliverymanMapper.getByPhone(phone);
		// 查询所有订单

		List<Order> orders = orderMapper.listByStatus(limit, status, subbranchId, deliveryman.getId());
		if (orders != null) {
			List<OrderVo> orderVos = new LinkedList<>();
			for (Order order : orders) {
				OrderVo orderVo = new OrderVo();
				BeanUtils.copyProperties(order, orderVo);
				// 根据订单查询订单详情
				List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
				int i = 0;
				for (OrderDetails orderDetails : list) {
					i += orderDetails.getGoods_count();
				}
				DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(order.getAddress_id());
				if (deliveryAddress != null) {
					orderVo.setLatitude(deliveryAddress.getLatitude());
					orderVo.setLongitude(deliveryAddress.getLongitude());
				}

				orderVo.setCount(i);
				orderVo.setOrderDetails(list);
				orderVos.add(orderVo);

			}
			return orderVos;
		}
		return null;

	}

	@Override
	public Achievement Achievements(Integer subbranchId, String time) throws Exception {
		long lt = new Long(time);
		Date date = new Date(lt);
		Achievement achievement = orderMapper.Achievements(subbranchId, DateUtil.getYearMonth(date));
		if (achievement.getTurnover() == null) {
			achievement.setTurnover(0);
		}
		return achievement;
	}

	@Override
	public int transferOrder(Long id, Long subbranchId) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		order.setSubbranch_id(subbranchId);
		order.setDeliveryman_id(null);
		order.setDeliveryman_name(null);
		// System.out.println("order:"+order);
		return orderMapper.updateByPrimaryKeySelective(order);
	}

	@Override
	public int updateToStatus(Long id, Integer status) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		order.setStatus(status);
		// 订单状态为2（正在配送）时发送短信
		if (status.equals(2)) {
			Deliveryman deliveryman = deliverymanMapper.selectByPrimaryKey(order.getDeliveryman_id());
			Map<String, String> map = new HashMap<>();
			// 订单号
			map.put("id", order.getId().toString());
			// 配送员姓名
			map.put("name", deliveryman.getName());
			// 配送员电话
			map.put("phone", deliveryman.getPhone());
			JYSMSUtil.sendMessage(order.getPhone(), "3529", map);
			// 订单状态等于3（已送达）
		} else if (status.equals(3)) {
			// 减库存
			repertoryService.reduceRepertory(id);

			// 判断该用户是否有推广人
			Long userId = order.getUser_id();
			Generalize generalize = generalizeMapper.getby_promoter(userId);

			// 查询出商品详情
			List<OrderDetails> listRecord = orderDetailsMapper.listByOrderId(order.getId());

			if (generalize != null) {

				BigDecimal count = new BigDecimal(0.00);
				for (OrderDetails orderDetails : listRecord) {
					Goods goods = goodsMapper.selectByPrimaryKey(orderDetails.getGoods_id());
					// 计算出每个商品的推广金总额
					count = count
							.add(goods.getGeneralize_money().multiply(new BigDecimal(orderDetails.getGoods_count())));

				}
				System.out.println("推广金" + count);
				// 给推广人增加推广金
				User user = userMapper.selectByPrimaryKey(generalize.getPromoter());
				if (user != null) {
					user.setGeneralize_money(user.getGeneralize_money().add(count));
					userMapper.updateByPrimaryKeySelective(user);
				}

				String wx_name = userMapper.selectByPrimaryKey(order.getUser_id()).getWx_name();
				// 订单明细表
				BonusInfo bonusInfo = new BonusInfo();
				// 订单编号是id
				bonusInfo.setOrder_id(id);
				// 推广人id
				bonusInfo.setUser_id(generalize.getPromoter());
				// 购买者
				bonusInfo.setPay_user_id(order.getUser_id());
				// order.getUser_id()
				// money是long？
				bonusInfo.setGeneralize_money(count);
				// 购买者微信昵称
				bonusInfo.setWx_name(wx_name);
				// 时间
				bonusInfo.setTime(new Date());
				bonusInfoMapper.insertSelective(bonusInfo);
				// System.out.println("bonusInfo:" + bonusInfo);

				// 提成明细表(收入)
				Deduction deduction = new Deduction();
				deduction.setMoney(count);
				deduction.setType((byte) 1);
				deduction.setUser_id(generalize.getPromoter());
				deduction.setMsg("收入:" + count + "元  " + "来源: " + wx_name);
				deduction.setTime(new Date());
				deductionMapper.insertSelective(deduction);
				// System.out.println(deduction);

			}
			for (OrderDetails orderDetails : listRecord) {
				Goods goods = goodsMapper.selectByPrimaryKey(orderDetails.getGoods_id());
				// 如果该商品有优惠券 给客户发券
				if (goods.getDiscounts() != 0) {
					Discounts discounts = discountsMapper.selectByPrimaryKey(Long.valueOf(goods.getDiscounts()));
					UserDiscounts record = new UserDiscounts();
					record.setDiscounts_id(discounts.getId());
					record.setStart_money(discounts.getStart_money());
					record.setSubtract_money(discounts.getSubtract_money());
					record.setStatus(0);
					record.setUser_id(userId);
					record.setTime(new Date());
					userDiscountsMapper.insertSelective(record);
				}
			}
			order.setEnd_time(new Date());
		}

		return orderMapper.updateByPrimaryKeySelective(order);
	}

	/**
	 * 
	 * 
	 * @Title: OrderServiceImpl.java
	 * 
	 * @Package com.yqwl.service.impl
	 * 
	 * @Description: 购物车生成订单
	 * 
	 * @author HanMeng
	 * 
	 * @date 2019年5月13日-下午3:33:00
	 */
	@Override
	public int insert(Order order, Long address_id, Long[] ids) throws Exception {
		// System.out.println("order；"+order);
		// System.out.println("address_id；"+address_id);
		// System.out.println("ids；"+ids);
		if (order.getRemarks().equals("null")) {
			order.setRemarks(null);
		}
		// 时间戳+随机数拼成订单号
		Long time = System.currentTimeMillis();
		Random rand = new Random();
		String num = String.valueOf((rand.nextInt(899) + 100));
		Long OrderId = Long.valueOf((time + num));
		// 查询收获地址
		DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(address_id);
		order.setId(OrderId);
		order.setAddress_id(address_id);

		order.setAddress(deliveryAddress.getLocation() + deliveryAddress.getAddress());
		order.setName(deliveryAddress.getName());
		order.setPhone(deliveryAddress.getPhone());
		order.setStatus(0);
		order.setTime(new Date());
		order.setType(order.getType());
		// order.setTotal_money(new BigDecimal(0));

		// 遍历购物车ID
		for (Long id : ids) {
			// System.out.println("id:"+id);
			OrderDetails orderDetails = new OrderDetails();
			// 查询购物车信息
			ShoppingTrolley shoppingTrolley = shoppingTrolleyMapper.selectByPrimaryKey(id);
			System.out.println("shoppingTrolley:" + shoppingTrolley);
			Goods goods = goodsMapper.selectByPrimaryKey(shoppingTrolley.getGoods_id());
			// 查询商品图标类型等于3（缩略图）的图片
			List<GoodsImages> list = goodsImagesMapper.getByGoodsIdAndType3(goods.getId());
			for (GoodsImages goodsImages : list) {
				orderDetails.setGoods_images(goodsImages.getUrl());
			}
			orderDetails.setOrder_id(OrderId);
			orderDetails.setGoods_id(goods.getId());
			orderDetails.setGoods_name(goods.getName());
			orderDetails.setGoods_count(shoppingTrolley.getConut());
			orderDetails.setGoods_original_money(goods.getOriginal_money());
			orderDetails.setGoods_describes(goods.getDescribes());
			orderDetails.setGoods_money(goods.getMoney());
			orderDetails.setGoods_is_ice(goods.getIs_ice());
			orderDetails.setGoods_is_discount(goods.getIs_discount());
			orderDetails.setGoods_discount_num(goods.getDiscount_num());

			User user = userMapper.selectByPrimaryKey(order.getUser_id());
			// // 如果商品是打折商品
			// if (goods.getIs_discount() == 1) {
			// // 如果用户首单状态是1 或者 2
			// if (user.getFirst_order() == 1 || user.getFirst_order() == 2) {
			// BigDecimal decimalCount = new
			// BigDecimal(shoppingTrolley.getConut());
			// order.setTotal_money(order.getTotal_money().add(goods.getMoney().multiply(decimalCount)));
			// } else {
			// // 用户是首单 生成订单，首单状态改为2 （首单优惠中间状态）
			//
			// user.setFirst_order(2);
			// userMapper.updateByPrimaryKey(user);
			// }
			// }
			// 如果商品是打折商品
			if (goods.getIs_discount() == 1 && user.getFirst_order() == 0) {

				user.setFirst_order(2);
				userMapper.updateByPrimaryKey(user);

			}
			// 判断冰镇数量
			if (shoppingTrolley.getLced() != 0) {
				// 判断时候有备注
				if (order.getRemarks() == null || order.getRemarks().equals("")) {
					order.setRemarks("冰镇" + goods.getName() + shoppingTrolley.getLced() + "件");
				} else {
					order.setRemarks(order.getRemarks() + ",冰镇" + goods.getName() + shoppingTrolley.getLced() + "件");
				}
			}
			orderDetailsMapper.insertSelective(orderDetails);
			shoppingTrolleyMapper.deleteByPrimaryKey(id);
		}
		// 判断是否使用优惠券
		if (order.getUser_discounts_id() != 0) {
			UserDiscounts record = new UserDiscounts();
			record.setId(order.getUser_discounts_id());
			record.setStatus(1);
			userDiscountsMapper.updateByPrimaryKeySelective(record);
		}
		int i = orderMapper.insertRecord(order);
		return i;
	}

	/**
	 * 
	 * @Title: findNoDistribution
	 * @description: 查询订单状态（0 未支付 1、未配送，2、配送中，3、已送达，4、退货）
	 *
	 * @param userId
	 * @return
	 * @return List<Order>
	 *
	 * @author HanMeng
	 * @createDate 2019年1月15日-下午5:12:56
	 */

	@Override
	public List<OrderVo> findOrder(Integer userId, Integer status, Integer limit) throws Exception {
		List<OrderVo> orders = orderMapper.findOrder(userId, status, limit);

		if (status == 0) {
			System.out.println("所有待支付订单:"+orders);
			//当前时间
			Date date = new Date();
			System.out.println("date"+date);
			System.out.println("dategetTime"+date.getTime());
			for (Order order : orders) {
				 
				// Long  date1 = df.parse(order.getTime());
				 long date1 = order.getTime().getTime();
				//Date date2 = df.parse(date);
				// 得到微秒级别的差值
				long diff = date.getTime() - date1;
				// 将级别提升到分钟
				long min = diff / (1000 * 60);
				System.out.println("min:"+min);
				//数据库该表只有一条数据  id写死了 呵呵呵
				Setting setting = settingMapper.selectByPrimaryKey(1L);
				//默认订单删除时间为30分钟
				long orderTimeMin = 30;
				if (setting != null) {
					 orderTimeMin  = Long.valueOf(setting.getMin());
				}			
				if (min > orderTimeMin ) {
					List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
					System.out.println("list:" + list);
					User user = userMapper.selectByPrimaryKey(order.getUser_id());
					// 循环删除订单详情
					for (OrderDetails orderDetails : list) {
						orderDetails = orderDetailsMapper.selectByPrimaryKey(orderDetails.getId());
						// 删除的订单有打折商品 且用户享受优惠中 改为初始状态
						if (orderDetails.getGoods_is_discount() == 1 && user.getFirst_order() == 2) {
							user.setFirst_order(0);
							userMapper.updateByPrimaryKey(user);
						}

						orderDetailsMapper.deleteByPrimaryKey(orderDetails.getId());
					}
				 orderMapper.deleteByPrimaryKey(order.getId());
				System.out.println("删除的orderid:"+order.getId());
				}
				
			}
			// 删除后再查
			orders = orderMapper.findOrder(userId, 0, limit);
			System.out.println(":再查："+orders);
			List<OrderVo> orderVos = new LinkedList<>();
			for (Order order : orders) {
				OrderVo orderVo = new OrderVo();
				BeanUtils.copyProperties(order, orderVo);
				List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());

				int i = 0;
				for (OrderDetails orderDetails : list) {

					i = i + orderDetails.getGoods_count();
				}
				orderVo.setCount(i);
				orderVo.setOrderDetails(list);
				orderVos.add(orderVo);
			}

			return orderVos;
		}
		List<OrderVo> orderVos = new LinkedList<>();
		for (Order order : orders) {
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());

			int i = 0;
			for (OrderDetails orderDetails : list) {

				i = i + orderDetails.getGoods_count();
			}
			orderVo.setCount(i);
			orderVo.setOrderDetails(list);
			orderVos.add(orderVo);
		}

		return orderVos;

	}

	@Override
	public int insertOrder(Order order, Long address_id, Long id, Integer count, Integer Lced) throws Exception {
		System.out.println("order:" + order);
		if (order.getRemarks().equals("null")) {
			order.setRemarks(null);
		}
		// 时间戳+随机数拼成订单号
		Long time = System.currentTimeMillis();
		Random rand = new Random();
		String num = String.valueOf((rand.nextInt(899) + 100));
		Long OrderId = Long.valueOf((time + num));

		DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(address_id);
		order.setId(OrderId);
		order.setAddress_id(address_id);
		order.setAddress(deliveryAddress.getLocation() + deliveryAddress.getAddress());
		order.setName(deliveryAddress.getName());
		order.setPhone(deliveryAddress.getPhone());
		order.setStatus(0);
		order.setTime(new Date());
		order.setType(order.getType());

		OrderDetails orderDetails = new OrderDetails();

		Goods goods = goodsMapper.selectByPrimaryKey(id);
		User user = userMapper.selectByPrimaryKey(order.getUser_id());
		// 如果商品是打折商品
		if (goods.getIs_discount() == 1) {
			// 如果用户首单状态是1 或者 2
			if (user.getFirst_order() == 1 || user.getFirst_order() == 2) {
				BigDecimal decimalCount = new BigDecimal(count);
				order.setTotal_money(goods.getMoney().multiply(decimalCount));
			} else {
				// 用户是首单 生成订单，首单状态改为2 （首单优惠中间状态）

				user.setFirst_order(2);
				userMapper.updateByPrimaryKey(user);
			}
		}

		List<GoodsImages> list = goodsImagesMapper.getByGoodsIdAndType3(goods.getId());
		for (GoodsImages goodsImages : list) {
			orderDetails.setGoods_images(goodsImages.getUrl());
		}
		orderDetails.setOrder_id(OrderId);
		orderDetails.setGoods_id(goods.getId());
		orderDetails.setGoods_name(goods.getName());
		orderDetails.setGoods_count(count);
		orderDetails.setGoods_original_money(goods.getOriginal_money());
		orderDetails.setGoods_describes(goods.getDescribes());
		orderDetails.setGoods_money(goods.getMoney());
		orderDetails.setGoods_is_ice(goods.getIs_ice());
		orderDetails.setGoods_is_discount(goods.getIs_discount());
		orderDetails.setGoods_discount_num(goods.getDiscount_num());
		if (Lced != 0) {
			if (order.getRemarks() == null) {
				order.setRemarks("冰镇" + goods.getName() + Lced + "件");
			} else {
				order.setRemarks(order.getRemarks() + ",冰镇" + goods.getName() + Lced + "件");
			}
		}
		orderDetailsMapper.insertSelective(orderDetails);
		if (order.getUser_discounts_id() != 0) {
			UserDiscounts record = new UserDiscounts();
			record.setId(order.getUser_discounts_id());
			record.setStatus(1);
			userDiscountsMapper.updateByPrimaryKeySelective(record);
		}
		int i = orderMapper.insertRecord(order);
		return i;
	}

	@Override
	public int deleteOrder(Long id) throws Exception {
		int i = 0;
		Order order = orderMapper.selectByPrimaryKey(id);
		// 判断订单状态等于0和3的时候可以删除订单
		if (order.getStatus() == 0 || order.getStatus() == 3) {
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(id);
			System.out.println("list:" + list);
			User user = userMapper.selectByPrimaryKey(order.getUser_id());
			// 循环删除订单详情
			for (OrderDetails orderDetails : list) {
				orderDetails = orderDetailsMapper.selectByPrimaryKey(orderDetails.getId());
				// 删除的订单有打折商品 且用户享受优惠中 改为初始状态
				if (orderDetails.getGoods_is_discount() == 1 && user.getFirst_order() == 2) {
					user.setFirst_order(0);
					userMapper.updateByPrimaryKey(user);

				}

				orderDetailsMapper.deleteByPrimaryKey(orderDetails.getId());
			}
			i = orderMapper.deleteByPrimaryKey(id);

		} else {
			i = -1;
		}
		return i;
	}

	/**
	 * 
	 * 
	 * @Title: OrderServiceImpl.java
	 * 
	 * @Package com.yqwl.service.impl
	 * 
	 * @Description: 修改订单状态（只有回调用到了这个）
	 * 
	 * @author HanMeng
	 * 
	 * @date 2019年2月12日-上午10:23:57
	 */
	@Override
	public int updateToOrderStatus(Long id, Integer status) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);

		order.setStatus(status);
		if (status == 1) {
			order.setTime(new Date());
		}
		System.out.println("订单状态为：" + status);
		return orderMapper.updateByPrimaryKeySelective(order);

	}

	@Override
	public Map<String, Object> listAll(String hm, Integer page, Integer limit) throws Exception {
		Integer resultCount = orderMapper.listAllCount(hm);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Order> result = orderMapper.listAll(hm, beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public OrderVo getByid(Long id) throws Exception {
		OrderVo orderVo = new OrderVo();
		Order order = orderMapper.selectByPrimaryKey(id);
		BeanUtils.copyProperties(order, orderVo);
		if (order.getUser_discounts_id() != null && order.getUser_discounts_id() != 0) {
			orderVo.setUserDiscounts(userDiscountsMapper.selectByPrimaryKey(order.getUser_discounts_id()));
		}
		orderVo.setOrderDetails(orderDetailsMapper.listByOrderId(id));

		return orderVo;
	}

	@Override
	public Map<String, Object> getByStatus(Integer status, Integer page, Integer limit) throws Exception {
		Integer resultCount = orderMapper.getByStatusCount(status);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Order> result = orderMapper.getByStatus(status, beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public int updateByDeliveryman(Order order) throws Exception {
		return orderMapper.updateByPrimaryKeySelective(order);
	}

	@Override
	public int rob(Long id, String phone) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(id);
		if (order.getDeliveryman_id() == null) {
			Deliveryman deliveryman = deliverymanMapper.getByPhone(phone);
			order.setDeliveryman_id(deliveryman.getId());
			order.setDeliveryman_name(deliveryman.getName());
			return orderMapper.updateByPrimaryKeySelective(order);
		}
		return -3;
	}

	/**
	 * 
	 * 
	 * @Title: OrderServiceImpl.java
	 * @Package com.yqwl.service.impl
	 * @Description: 抢单
	 * @author HanMeng
	 * @date 2019年3月11日-上午10:25:01
	 */
	@Override
	public List<OrderVo> robOrder(Integer limit, Integer subbranchId) throws Exception {
		// 查询所有订单
		List<Order> orders = orderMapper.robOrder(limit, subbranchId);
		List<OrderVo> orderVos = new LinkedList<>();
		for (Order order : orders) {
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			// 根据订单查询订单详情
			List<OrderDetails> list = orderDetailsMapper.listByOrderId(order.getId());
			int i = 0;
			for (OrderDetails orderDetails : list) {
				i += orderDetails.getGoods_count();
			}
			DeliveryAddress deliveryAddress = DeliveryAddressMapper.selectByPrimaryKey(order.getAddress_id());
			if (deliveryAddress != null) {
				orderVo.setLatitude(deliveryAddress.getLatitude());
				orderVo.setLongitude(deliveryAddress.getLongitude());
			}
			orderVo.setCount(i);
			orderVo.setOrderDetails(list);
			orderVos.add(orderVo);
		}
		return orderVos;
	}

	@Override
	public Map<String, Object> getByCondition(Long subbranch_id, String deliveryman_name, Long startTime, Long endTime,
			Integer page, Integer limit) throws Exception {
		Date startTime1 = null;
		Date endTime1 = null;
		if (startTime != null && endTime != null) {
			startTime1 = new Date(startTime);
			endTime1 = new Date(endTime);
		}
		Integer resultCount = orderMapper.getByConditionCount(subbranch_id, deliveryman_name, startTime1, endTime1);
		Integer beginPageIndex = ((page - 1) * limit);
		List<Order> result = orderMapper.getByCondition(subbranch_id, deliveryman_name, startTime1, endTime1,
				beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public Map<String, Integer> countStatus(Long subbranchId, Long deliveryman_id) throws Exception {
		Integer status1RobCount = orderMapper.robOrderCount(subbranchId);
		Integer status1Count = orderMapper.OrderStatus1Count(subbranchId, deliveryman_id);
		Integer status2Count = orderMapper.OrderStatus2Count(subbranchId, deliveryman_id);
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("status1RobCount", status1RobCount);
		map.put("status1Count", status1Count);
		map.put("status2Count", status2Count);
		return map;

	}

	/**
	 * 
	 * 
	 * @Title: OrderServiceImpl.java
	 * 
	 * @Package com.yqwl.service.impl
	 * 
	 * @Description: pc后台订单管理定时查询
	 * 
	 * @author HanMeng
	 * 
	 * @date 2019年4月15日-下午1:52:52
	 */
	@Override
	public Integer orderCount() {
		return orderMapper.orderCount();
	}

}
