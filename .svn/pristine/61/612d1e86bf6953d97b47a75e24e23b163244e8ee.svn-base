package com.yqwl.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.service.AdministratorService;
import com.yqwl.service.SubbranchService;

/**
 * 
 * @ClassName: AdministratorController
 * @description 管理員控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:25:55
 */
@Controller
@RequestMapping("admin")
@Scope("prototype")
public class AdministratorController {

	@Resource
	private AdministratorService administratorService;
	@Resource
	private SubbranchService subbranchService;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String list(HttpSession session) {
		Administrator admin = (Administrator) session.getAttribute("login_admin");
		if (admin != null) {
			return "LiquorAdmin/view/systemAdmin/systemAdmin";
		} else {
			return "redirect:/login/toLogin.action";
		}
	}

	@RequestMapping(value = "msg", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String msg() {
		return "LiquorAdmin/templates/msg";
	}

	@RequestMapping(value = "branchMoney", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String branchMoney(HttpSession session) {
		
		Administrator admin = (Administrator) session.getAttribute("login_admin");
		if (admin != null) {
			return "LiquorAdmin/view/branchAdmin/branchMoney";
		} else {
			return "redirect:/login/toLogin.action";
		}
	}

	@RequestMapping(value = "toLogin", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String toLogin() {
		return "LiquorAdmin/login/login";
	}

	/**
	 * 
	 * @Title: updatePWD
	 * @description 修改密码
	 * @param oldPWD
	 * @param newPWD
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:31:06
	 */
	@ResponseBody
	@RequestMapping(value = "updatePWD", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String updatePWD(String oldPWD, String newPWD, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = administratorService.updatePWD(oldPWD, newPWD, admin);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "密码错误";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listLog
	 * @description 后台显示分店登陆日志
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月28日 下午2:36:10
	 */
	@ResponseBody
	@RequestMapping(value = "listLog", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listLog(Integer page, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> log = subbranchService.listLog(page, limit);
				if (log.size() != 0) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, log);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	@ResponseBody
	@RequestMapping(value = "turnover", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String turnover(Integer page, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> result = subbranchService.turnover(page, limit);
				if (result.size() != 0) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, result);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
