package com.yqwl.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Goods;
import com.yqwl.vo.GoodsVo;

public interface GoodsService {
	/**
	 * 
	 * @Title: insert
	 * @description 新增
	 * @param goods
	 * @param admin
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月3日 上午9:55:09
	 */
	Long insert(Goods goods, Administrator admin) throws Exception;

	/**
	 * 
	 * @Title: listAll
	 * @description 分页加模糊查询（Name==null时查询全部）
	 * @param page
	 * @param limit
	 * @param name
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @author likai
	 * @createDate 2019年1月3日 上午9:55:21
	 */
	Map<String, Object> listAll(Integer page, Integer limit, String name) throws Exception;

	/**
	 * 
	 * @Title: listAllByType
	 * @description 分页根据商品类型查询
	 * @param page
	 * @param limit
	 * @param type
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @author likai
	 * @createDate 2019年1月3日 上午9:56:14
	 */
	Map<String, Object> listAllByType(Integer page, Integer limit, Integer type) throws Exception;

	/**
	 * 
	 * 
	 * @Title: update
	 * @description 修改
	 * @param goods
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月3日 上午9:56:39
	 */
	Long update(Goods goods) throws Exception;

	/**
	 * 
	 * @Title: getById
	 * @description 根据ID查询
	 * @param id
	 * @return
	 * @throws Exception
	 *             GoodsVo
	 * @author likai
	 * @createDate 2019年1月3日 上午9:56:52
	 */
	GoodsVo getById(Long id) throws Exception;

	/**
	 * 
	 * @Title: delete
	 * @description 根据ID删除
	 * @param id
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月3日 上午9:57:14
	 */
	int delete(Long id) throws Exception;
	/**
	 * 
	 * @Title: findAll
	 * @description 查询所有商品
	 * @return
	 * @throws Exception
	 * List<Goods>
	 * @author likai
	 * @createDate 2019年1月21日 下午3:05:46
	 */
	List<Goods> findAll() throws Exception;

	/**
	 * 
	 * @Title: findHot
	 * @description: 查询爆款商品
	 *
	 * @return
	 * @return List<Goods>
	 *
	 * @author HanMeng
	 * @createDate 2019年1月15日-下午3:38:40
	 */
	List<GoodsVo> findHot() throws Exception;

	

	List<GoodsVo> listByType(Integer type, Integer limit) throws Exception;

	List<GoodsVo> listGoodsByName(String name, Integer limit) throws Exception;

	int addGoodsImg(MultipartFile[] files, Integer type, Long id) throws Exception;

	int deleteGoodsImg(Long id) throws Exception;
	/**
	 * 
	 * @Title: findIsDiscounts
	 * @description: 查询折扣商品（首单）
	 *
	 * @return    
	 * @return List<Goods>   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月29日-下午4:46:49
	 */
	List<Goods> findIsDiscounts();

	List<GoodsVo> listByTypeId(Integer typeId) throws Exception;

}
