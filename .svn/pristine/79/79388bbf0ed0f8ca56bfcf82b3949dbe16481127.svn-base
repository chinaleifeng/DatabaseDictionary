package com.yqwl.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.dao.GoodsFashionMapper;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Goods;
import com.yqwl.pojo.GoodsFashion;
import com.yqwl.service.GoodsService;
import com.yqwl.vo.GoodsVo;
/**
 * 
 * @ClassName: GoodsController
 * @description 商品控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:26:41
 */
@Controller
@RequestMapping("goods")
@Scope("prototype")
public class GoodsController {

	@Resource
	private GoodsService goodsService;
	
	@Resource
	private GoodsFashionMapper goodsFashionMapper;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String list() {
		return "LiquorAdmin/view/waresAdmin/waresAdmin";
	}

	@RequestMapping(value = "add", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String add() {
		return "LiquorAdmin/view/waresAdmin/waresAdminAdd";
	}

	@RequestMapping(value = "info", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String info() {
		return "LiquorAdmin/view/waresAdmin/waresAdminInfo";
	}

	/**
	 * 
	 * @Title: addGoods
	 * @description 增加商品
	 * @param goods
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:46:08
	 */
	@ResponseBody
	@RequestMapping(value = "insert", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String addGoods(Goods goods, HttpSession session,HttpServletRequest request) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				
				Long id = goodsService.insert(goods, admin);
				if (id != 0) {
					ArrayList<String> fashionslist = new ArrayList<String>();
					String parameter = request.getParameter("fashion");
					System.out.println("fashions:" + parameter);
					fashionslist.add(parameter);
					for (String fashion : fashionslist) {
						System.out.println("遍历："+fashion);
						GoodsFashion goodsFashion = new GoodsFashion();
						goodsFashion.setFashion_id(Long.valueOf(fashion));
						goodsFashion.setGoods_id(id);
						goodsFashion.setTime(new Date());
						
						int num = goodsFashionMapper.insertSelective(goodsFashion);
						if(num > 0){
							code = 0;
							msg = "新增成功";
							return FastJsonUtil.getResponseJson(code, msg, id);
						}
						
					}

				}
				
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
				
				
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: deleteGoodsImg
	 * @description 删除图片
	 * @param id
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月21日 下午2:58:46
	 */
	@ResponseBody
	@RequestMapping(value = "deleteGoodsImg", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String deleteGoodsImg(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = goodsService.deleteGoodsImg(id);
				if (i != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: addGoodsImg
	 * @description 添加商品图片
	 * @param file
	 * @param type
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月21日 下午1:41:16
	 */
	@ResponseBody
	@RequestMapping(value = "addGoodsImg", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String addGoodsImg(@RequestParam(value = "file", required = true) MultipartFile[] file, Integer type,
			Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = goodsService.addGoodsImg(file, type, id);
				if (i != 0) {
					code = 0;
					msg = "新增成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: update
	 * @description 修改商品信息
	 * @param id
	 * @param status
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:46:47
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String update(Goods goods, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = goodsService.update(goods);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listAll
	 * @description 分页加模糊查询（Name==null时查询全部）
	 * @param page
	 * @param limit
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:47:09
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "page", required = true) Integer page, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> adminList = goodsService.listAll(page, limit, name);
				if (adminList != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, adminList);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listAllByType
	 * @description 根据商品类型查询
	 * @param page
	 * @param limit
	 * @param type
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:47:39
	 */
	@ResponseBody
	@RequestMapping(value = "listAllByType", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAllByType(Integer page, Integer limit, Integer type, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> adminList = goodsService.listAllByType(page, limit, type);
				if (adminList != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, adminList);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: getById
	 * @description 通过ID查询
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月2日 下午2:47:57
	 */
	@ResponseBody
	@RequestMapping(value = "getById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				GoodsVo goods = goodsService.getById(id);
				if (goods != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, goods);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: delete
	 * @description 根据主键删除
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月3日 上午9:54:49
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String delete(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int goods = goodsService.delete(id);
				if (goods != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, goods);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: findAll
	 * @description 查询所有商品
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月15日 上午11:31:10
	 */
	@ResponseBody
	@RequestMapping(value = "findAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findAll(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<Goods> goods = goodsService.findAll();
				if (goods != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, goods);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
