package com.yqwl.controller.appBack;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.pojo.Order;
import com.yqwl.pojo.Subbranch;
import com.yqwl.pojo.User;
import com.yqwl.push.Demo;
import com.yqwl.service.OrderService;
import com.yqwl.service.SubbranchService;
import com.yqwl.vo.OrderVo;

/**
 * 
 * @ClassName: BackOrderController
 * @description 分店订单控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:31:14
 */
@Controller
@RequestMapping("backOrder")
@Scope("prototype")
public class BackOrderController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private OrderService orderService;
	@Resource
	private SubbranchService subbranchService;
	@Resource
	private DeliverymanMapper deliverymanMapper;
	/**
	 * 
	 * @Title: listByStatus
	 * @description 根据状态分页查询订单
	 * @param subbranchId
	 * @param limit
	 * @param status
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:33:57
	 */
	@ResponseBody
	@RequestMapping(value = "listByStatus", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listByStatus(Integer subbranchId,String phone,Integer limit, Integer status, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {

		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				List<OrderVo> list = orderService.listByStatus(limit, status, subbranchId,phone);
				if (list != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, list);
				}
				code = 1;
				msg = "没有订单";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: transferOrder
	 * @description 转单
	 * @param id
	 * @param subbranchId
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:34:21
	 */
	@ResponseBody
	@RequestMapping(value = "transferOrder", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String transferOrder(Long id, Long subbranchId, HttpSession session) {

		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				int i = orderService.transferOrder(id, subbranchId);
				// 消息推送
				Demo demo = new Demo("5c94586a0cafb21a0700119c", "4tnpcx3pnv47w2ekq59bnrmk8oikf0il");				
				try {
					String  subbranchName = subbranchService.selectByPrimaryKey(subbranchId).getName();
					// 自定义
					// demo.sendAndroidCustomizedcastFile();
					 demo.sendAndroidBroadcast(subbranchName);
					// demo.sendAndroidGroupcast();
					//demo.sendAndroidCustomizedcast();
					// demo.sendAndroidFilecast();
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (i != 0) {
					code = 0;
					msg = "转单成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "转单失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: updateToStatus
	 * @description 修改订单状态
	 * @param id
	 * @param status
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:34:32
	 */
	@ResponseBody
	@RequestMapping(value = "updateToStatus", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String updateToStatus(Long id, Integer status, HttpSession session) {

		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				int i = orderService.updateToStatus(id, status);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: rob
	 * @description: 抢单
	 *
	 * @param id
	 * @param phone
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月20日-下午6:12:53
	 */
	@ResponseBody
	@RequestMapping(value = "rob", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String rob(Long id,String phone, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				int i = orderService.rob(id, phone);
				if (i==-3) {
					code = 1;
					msg = "抢单失败：订单已备抢";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				if (i != 0) {
					code = 0;
					msg = "抢单成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "抢单失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: robOrder
	 * @description: 查询支付成功没有接单的订单
	 *
	 * @param subbranchId
	 * @param limit
	 * @param request
	 * @param response
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月20日-下午6:14:59
	 */
	@ResponseBody
	@RequestMapping(value = "robOrder", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String robOrder(Integer subbranchId, Integer limit, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				List<OrderVo> list = orderService.robOrder(limit,subbranchId);
				if (list != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, list);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: updateByDeliveryman
	 * @description: 转单至本店其他人
	 *
	 * @param Order
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月23日-下午3:43:33
	 */
	@ResponseBody
	@RequestMapping(value = "updateByDeliveryman", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String updateByDeliveryman(Order Order, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				int i = orderService.updateByDeliveryman(Order);
				if (i != 0) {
					code = 0;
					msg = "分配成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "分配失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: getOrder
	 * @description: 查询订单根据订单编号
	 *
	 * @param session
	 * @param orderId
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月23日-下午3:43:13
	 */
	@ResponseBody
	@RequestMapping(value = "getOrder", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getOrder(HttpSession session,Long orderId) {
		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
			if (boss != null) {
				OrderVo order = orderService.getByid(orderId);
				if (order != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, order);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: countStatus
	 * @description: 查询各种状态的条数
	 *
	 * @param session
	 * @param subbranchId
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月23日-下午3:42:43
	 */
	@ResponseBody
	@RequestMapping(value = "countStatus", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String countStatus(HttpSession session,Long subbranchId,String phone) {
		int code = 0;
		String msg = null;
		try {
			Subbranch boss = (Subbranch) session.getAttribute("boss");
		//	String deliveryId = (String) session.getAttribute("deliverymanId");
			Long  deliverymanIds = deliverymanMapper.getByPhone(phone).getId();
			if (boss != null) {
				Map<String, Integer> order = orderService.countStatus(subbranchId,deliverymanIds);
				if (order != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, order);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
