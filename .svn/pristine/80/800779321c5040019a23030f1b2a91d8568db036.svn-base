package com.yqwl.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.yqwl.pojo.Order;
import com.yqwl.vo.Achievement;
import com.yqwl.vo.OrderVo;

public interface OrderService {
	/**
	 * 
	 * @Title: listByStatus
	 * @description 根据状态查询订单
	 * @param limit
	 * @param status
	 * @param subbranchId
	 * @return
	 * @throws Exception
	 *             List<OrderVo>
	 * @author likai
	 * @param phone
	 * @createDate 2019年1月9日 上午10:36:48
	 */
	List<OrderVo> listByStatus(Integer page,Integer limit, Integer status, Integer subbranchId, String phone) throws Exception;

	/**
	 * 
	 * @Title: Achievements
	 * @description 根据月份查询业绩
	 * @param subbranchId
	 * @param time
	 * @return
	 * @throws Exception
	 *             Achievement
	 * @author likai
	 * @createDate 2019年1月9日 上午10:37:03
	 */
	Achievement Achievements(Integer subbranchId, String time) throws Exception;

	/**
	 * 
	 * @Title: transferOrder
	 * @description 转单
	 * @param id
	 * @param subbranchId
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月9日 上午10:37:31
	 */
	int transferOrder(Long id, Long subbranchId) throws Exception;

	/**
	 * 
	 * @Title: updateToStatus
	 * @description 修改订单状态（含其他）
	 * @param id
	 * @param status
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月9日 上午10:37:41
	 */
	int updateToStatus(Long id, Integer status) throws Exception;

	/**
	 * 
	 * @Title: updateToOrderStatus
	 * @description: 修改订单状态（只修改状态）
	 *
	 * @param id
	 * @param status
	 * @return
	 * @throws Exception
	 * @return int
	 *
	 * @author HanMeng
	 * @createDate 2019年2月12日-上午10:21:57
	 */
	int updateToOrderStatus(Long id, Integer status) throws Exception;

	/**
	 * 
	 * @Title: insert
	 * @description 新增订单
	 * @param order
	 * @param address_id
	 * @param ids
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月21日 下午3:06:25
	 */
	int insert(Order order, Long address_id, Long[] ids) throws Exception;

	/**
	 * 
	 * @Title: findOrder
	 * @description: 查询订单状态（0 未支付 1、未配送，2、配送中，3、已送达，4、退货）
	 *
	 * @param userId
	 * @return
	 * @return List<Order>
	 *
	 * @author HanMeng
	 * @createDate 2019年1月15日-下午5:12:56
	 */
	List<OrderVo> findOrder(Integer userId, Integer status, @Param("limit") Integer limit) throws Exception;

	/**
	 * 
	 * @Title: insertOrder
	 * @description 直接生成订单跳过购物车
	 * @param order
	 * @param address_id
	 * @param id
	 * @param count
	 * @param Lced
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月30日 上午10:42:34
	 */
	int insertOrder(Order order, Long address_id, Long id, Integer count, Integer Lced) throws Exception;

	/**
	 * 
	 * @Title: deleteOrder
	 * @description 删除
	 * @param id
	 * @return
	 * @throws Exception
	 *             int
	 * @author likai
	 * @createDate 2019年1月30日 上午10:42:49
	 */
	int deleteOrder(Long id) throws Exception;

	Map<String, Object> listAll(String hm, Integer page, Integer limit) throws Exception;

	OrderVo getByid(Long id) throws Exception;

	Map<String, Object> getByStatus(Integer status, Integer page, Integer limit) throws Exception;

	int updateByDeliveryman(Order order) throws Exception;

	int rob(Long id, String phone) throws Exception;

	List<OrderVo> robOrder(Integer limit, Integer subbranchId) throws Exception;

	Map<String, Object> getByCondition(Long subbranch_id, String deliveryman_name, Long startTime, Long endTime,
			Integer page, Integer limit) throws Exception;
	/**
	 * 
	 * @Title: countStatus
	 * @description: 查询各种状态的各有多少条
	 *
	 * @return
	 * @throws Exception    
	 * @return List<Order>   
	 *
	 * @author HanMeng
	 * @createDate 2019年3月23日-上午10:47:27
	 */
	Map<String, Integer> countStatus(Long subbranchId,Long deliveryman_id)throws Exception;
	/**
	 * 
	 * @Title: orderCount
	 * @description: 查询新订单数量
	 *
	 * @return    
	 * @return Integer   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月15日-下午1:51:03
	 */
	Integer orderCount();
	/**
	 * 
	 * @Title: deleteOrderPC
	 * @description: 
	 *
	 * @param status
	 * @return
	 * @throws Exception    
	 * @return List<OrderVo>   
	 *
	 * @author HanMeng
	 * @createDate 2019年6月20日-上午11:26:07
	 */
	List<OrderVo> deleteOrderPC(Integer status) throws Exception;
}
