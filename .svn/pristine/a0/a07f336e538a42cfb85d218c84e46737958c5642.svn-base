package com.yqwl.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Fashion;
import com.yqwl.service.FashionService;
/**
 * 
 * @ClassName: FashionController
 * @description 首页推荐模块控制器
 * @author likai
 * @createDate 2019年1月28日 下午1:55:25
 */
@Controller
@RequestMapping("/fashion")
@Scope("prototype")
public class FashionController {
	@Resource
	private FashionService fashionService;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String list() {
		return "LiquorAdmin/view/waresAdmin/moduleAdmin";
	}
	/**
	 * 
	 * @Title: listAll
	 * @description 查询所有首页推荐活动
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月28日 下午1:53:35
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<Fashion> fashions = fashionService.listAll();
				if (fashions != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, fashions);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: insert
	 * @description 新增活动
	 * @param session
	 * @param Fashion
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月28日 下午1:54:18
	 */
	@ResponseBody
	@RequestMapping(value = "insert", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String insert(HttpSession session, Fashion Fashion) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = fashionService.insert(Fashion);
				if (i != 0) {
					code = 0;
					msg = "新增成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: getByid
	 * @description 根据ID查询活动详情
	 * @param session
	 * @param id
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月28日 下午1:54:40
	 */
	@ResponseBody
	@RequestMapping(value = "getByid", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getByid(HttpSession session, Long id) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Fashion fashion = fashionService.getByid(id);
				if (fashion != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, fashion);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: delete
	 * @description 删除推荐活动
	 * @param session
	 * @param id
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月28日 下午1:54:59
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String delete(HttpSession session, Long id) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = fashionService.delete(id);
				if (i != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: update
	 * @description 修改推荐活动
	 * @param session
	 * @param Fashion
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月28日 下午1:55:10
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String update(HttpSession session, Fashion Fashion) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = fashionService.update(Fashion);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

}
