package com.yqwl.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Discounts;
import com.yqwl.service.DiscountsService;
/**
 * 
 * @ClassName: DiscountsController
 * @description 优惠券控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:26:25
 */
@Controller
@RequestMapping("discounts")
@Scope("prototype")
public class DiscountsController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Resource
	private DiscountsService discountsService;
	
	
	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String list(HttpSession session) {
		Administrator admin = (Administrator) session.getAttribute("login_admin");
		if (admin != null) {
			return "LiquorAdmin/view/waresAdmin/coupon";
		} else {
			return "redirect:/login/toLogin.action";
		}
	}
	@RequestMapping(value = "add", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String add() {
		return "LiquorAdmin/view/waresAdmin/couponAdd";
	}
	
	@RequestMapping(value = "info", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String info() {
		return "LiquorAdmin/view/waresAdmin/couponInfo";
	}
	/**
	 * 
	 * @Title: insert
	 * @description 添加优惠卷
	 * @param discounts
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:32:00
	 */
	@ResponseBody
	@RequestMapping(value = "insert", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String insert(Discounts discounts, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = discountsService.insert(discounts);
				if (i != 0) {
					code = 0;
					msg = "新增成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: listAll
	 * @description 分页查询所有优惠券
	 * @param page
	 * @param limit
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:32:27
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(Integer page, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> map = discountsService.listAll(page, limit);
				if (map != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, map);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: findAll
	 * @description 查询所有优惠券
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月9日 上午10:32:46
	 */
	@ResponseBody
	@RequestMapping(value = "findAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findAll(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<Discounts> discounts = discountsService.findAll();
				if (discounts != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, discounts);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: update
	 * @description: 修改优惠券信息
	 *
	 * @param discounts
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月5日-上午9:08:59
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String update(Discounts discounts, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = discountsService.update(discounts);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "修改失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: findById
	 * @description: 根据主键查找优惠券
	 *
	 * @param id
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月5日-上午9:46:11
	 */
	@ResponseBody
	@RequestMapping(value = "findById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Discounts discounts = discountsService.findById(id);
				if (discounts != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, discounts);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: findByFirstOrder
	 * @description: 查询是否有首单优惠券
	 *
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月17日-下午3:53:13
	 */
	@ResponseBody
	@RequestMapping(value = "findByFirstOrder", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findByFirstOrder( HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<Discounts> i = discountsService.findByFirstOrder();
				if (i.size() >=  0) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "没有设置首单优惠券 或查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
