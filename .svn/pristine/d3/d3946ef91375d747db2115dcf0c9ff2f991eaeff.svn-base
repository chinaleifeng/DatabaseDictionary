package com.yqwl.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.GoodsType;
import com.yqwl.service.GoodsTypeService;
/**
 * 
 * @ClassName: GoodsTypeController
 * @description 商品类别控制器
 * @author likai
 * @createDate 2019年1月21日 下午4:27:02
 */
@Controller
@RequestMapping("goodsType")
@Scope("prototype")
public class GoodsTypeController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	private GoodsTypeService goodsTypeService;
	/**
	 * 
	 * @Title: listAll
	 * @description 查询所有商品分类
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月3日 上午9:59:11
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(HttpSession session){
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				JSONArray GoodsType = goodsTypeService.listAll();
				if (GoodsType != null ) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, GoodsType);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	
	/**
	 * 
	 * @Title: insert
	 * @description 新增商品类型
	 * @param goodsType
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月3日 下午2:28:07
	 */
	@ResponseBody
	@RequestMapping(value = "insert", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String insert(GoodsType goodsType,HttpSession session){
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i  = goodsTypeService.insert(goodsType);
				if (i != 0 ) {
					code = 0;
					msg = "新增成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	
	/**
	 * 
	 * @Title: delete
	 * @description 用一句话描述这个方法的作用
	 * @param id
	 * @param session
	 * @return
	 * String
	 * @author likai
	 * @createDate 2019年1月3日 下午2:53:10
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String delete(Long id,HttpSession session){
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				String result  = goodsTypeService.delete(id);
				return result;
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "addImg", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String addGoodsImg(@RequestParam(value = "file", required = true) MultipartFile file,Long id,HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = goodsTypeService.addGoodsImg(file,id);
				if (i != 0) {
					code = 0;
					msg = "上传成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "上传失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
