package com.yqwl.controller.appFront;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Carousel;

import com.yqwl.service.CarouselService;

/**
 * 
 *
 * @ClassName: FrontCarouselController
 *
 * @description 轮播图控制器
 *
 * @author HanMeng
 * @createDate 2019年4月28日-下午3:45:27
 */
@Controller
@RequestMapping("/frontCarousel")
@Scope("prototype")
public class FrontCarouselController {
	@Resource
	private CarouselService carouselService;

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 
	 * @Title: listAll
	 * @description: 查询所有轮播图
	 *
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午3:16:08
	 */
	@ResponseBody
	@RequestMapping(value = "findAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findAll() {
		int code = 0;
		String msg = null;
		try {
			List<Carousel> carousel = carouselService.findAll();
			if (carousel != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, carousel);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

}
