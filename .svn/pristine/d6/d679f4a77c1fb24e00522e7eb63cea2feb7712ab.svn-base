package com.yqwl.controller.appBack;

import com.yqwl.dao.UserMapper;
import com.yqwl.pay.Json;
import com.yqwl.pay.WxPayConfig;
import com.yqwl.pay.utils.PayUtil;
import com.yqwl.pay.utils.RefundUtil;
import com.yqwl.pojo.Order;
import com.yqwl.pojo.Subbranch;
import com.yqwl.pojo.User;
import com.yqwl.pojo.WxpayNotify;
import com.yqwl.push.Demo;
import com.yqwl.service.OrderService;
import com.yqwl.service.RepertoryService;
import com.yqwl.service.SubbranchService;
import com.yqwl.service.UserService;
import com.yqwl.service.WxpayNotifyService;
import com.yqwl.service.WxpayRefundService;
import com.yqwl.service.WxpayUnifiedOrderService;
import com.yqwl.vo.OrderVo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.weixin4j.WeixinSupport;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 *
 * @ClassName: WeixinController
 *
 * @description 微信 支付 、回调、 退款、
 *
 * @author HanMeng
 * @createDate 2019年2月1日-上午9:23:09
 */
@RequestMapping("/weixin")
@RestController
public class WeixinController extends WeixinSupport {

	@Resource
	private OrderService orderService;
	@Resource
	private SubbranchService subbranchService;
	@Resource
	private RepertoryService repertoryService;
	@Resource
	private WxpayNotifyService wxpayNotifyService;
	@Resource
	private UserService userService;
	@Resource
	private UserMapper userMapper;
	@Resource
	private WxpayUnifiedOrderService wxpayUnifiedOrderService;
	@Resource
	private WxpayRefundService wxpayRefundService;
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * @Title: wxPay
	 * @description:发起微信支付
	 *
	 * @param openid
	 * @param id
	 *            订单编号（string）
	 * @param total_money
	 *            总金额
	 * @param ids
	 *            订单编号（int） 和id一样只是类型不一样
	 * @param status
	 *            订单状态
	 * @param request
	 * @return
	 * @return Json
	 *
	 * @author HanMeng
	 * @createDate 2019年1月28日-上午10:47:13
	 */
	@RequestMapping("wxPay")

	public Json wxPay(String openid, String id, String total_money, Long ids, Integer status,
			HttpServletRequest request) {

		Json json = new Json();
		try {
			json = wxpayUnifiedOrderService.wxPay(openid, id, total_money, ids, status, request);

		} catch (Exception e) {
			e.printStackTrace();
			json.setSuccess(false);
			json.setMsg("发起失败");
			logger.error(e.getMessage(), e);
		}
		return json;
	}

	/**
	 * 
	 * @Title: wxNotify
	 * @description: 微信支付回调
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 * @return void
	 *
	 * @author HanMeng
	 * @createDate 2019年2月13日-下午5:58:26
	 */
	@RequestMapping(value = "/wxNotify")
	public String wxNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream()));
		String line = null;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		br.close();
		// sb为微信返回的xml
		String notityXml = sb.toString();
		String resXml = "";

		Map map = PayUtil.doXMLParse(notityXml);
		System.out.println("支付成功回调接收到的报文：" + notityXml);

		String cash_fee = (String) map.get("cash_fee");

		String openid = (String) map.get("openid");

		// 订单号
		String out_trade_no = (String) map.get("out_trade_no");

		String result_code = (String) map.get("result_code");
		String returnCode = (String) map.get("return_code");
		String total_fee = (String) map.get("total_fee");
		String transaction_id = (String) map.get("transaction_id");
		// 根据openid查找userid
		User user = userService.findByOpenId(openid);
		// System.out.println("user回调" + user);
		//看回调表有没有回调过 有则不插入
		WxpayNotify record = wxpayNotifyService.getTransaction_id(transaction_id);
		// System.out.println("record:" + record);
		if (record == null) {
			record = new WxpayNotify();
			if (user != null) {
				record.setUser_id(Integer.parseInt(String.valueOf(user.getId())));
			}
			record.setCash_fee(cash_fee);
			record.setOpenid(openid);
			record.setOut_trade_no(out_trade_no);
			record.setResult_code(result_code);
			record.setReturn_code(returnCode);
			record.setCash_fee(cash_fee);
			record.setTotal_fee(total_fee);
			record.setTransaction_id(transaction_id);
			record.setTime(new Date());
			wxpayNotifyService.insertSelective(record);

			String result = record.getResult_code();
	//		System.out.println("result::::" + result);
			if (result != null) {

				if (result == "SUCCESS" || result_code.equals(result_code)) {
					
					// 分配到 店铺
					Long i = subbranchService.StepThree(Long.valueOf(out_trade_no));
					if (i != 0) {
						System.out.println("分配店铺成功:" + i);
					}
					// 更新订单状态为未配送
					orderService.updateToOrderStatus(Long.valueOf(out_trade_no), 1);
					System.out.println("更新订单状态为未配送");
					// 消息推送
					Demo demo = new Demo("5c94586a0cafb21a0700119c", "4tnpcx3pnv47w2ekq59bnrmk8oikf0il");
					
					
					
					
					
					
					
					
					try {
						String  subbranchName = subbranchService.selectByPrimaryKey(i).getName();
						// 自定义
						// demo.sendAndroidCustomizedcastFile();
						 demo.sendAndroidBroadcast(subbranchName);
						// demo.sendAndroidGroupcast();
						//demo.sendAndroidCustomizedcast();
						// demo.sendAndroidFilecast();
						
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					OrderVo	order = orderService.getByid(Long.valueOf(out_trade_no));
					if (order.getType() == 2) {
						balance(openid,order);
					} 
					
					// // 修改库存
					// repertoryService.reduceRepertory(Long.valueOf(out_trade_no));
				}

				if ("SUCCESS".equals(returnCode)) {
					// 验证签名是否正确
					if (PayUtil.verify(PayUtil.createLinkString(map), (String) map.get("sign"), WxPayConfig.key,
							"UTF8")) {

					}

					/** 此处添加自己的业务逻辑代码end **/

					// 通知微信服务器已经支付成功
					resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
							+ "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
					System.out.println("微信服务器已经支付成功resXml:" + resXml);
				} else {
					System.out.println("数据库没有result_code");
				}
			}
		} else {
			resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
					+ "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";

		}

		System.out.println("微信支付回调数据结束");

		BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
		out.write(resXml.getBytes());
		out.flush();
		out.close();
		return transaction_id;
	}

	/**
	 * 
	 * @Title: doPost
	 * @description: 退款
	 *
	 * @param out_trade_no
	 * @param transaction_id
	 * @param total_fee
	 * @param refund_fee
	 * @param req
	 * @param resp
	 * @throws ServletException
	 * @throws IOException
	 * @return void
	 *
	 * @author HanMeng
	 * @createDate 2019年2月13日-下午6:11:38
	 */
	@RequestMapping(value = "/refund")
	public Map<String, Object> doPost(String out_trade_no, String transaction_id, String total_fee, String refund_fee,
			HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		// 根据商户单号查找出微信单号
		WxpayNotify wxpayNotify = wxpayNotifyService.selectByout_trade_no(out_trade_no);
		if (wxpayNotify != null) {
			transaction_id = wxpayNotify.getTransaction_id();
		} else {
			System.out.println("微信账单暂未回调");
		}

		Map<String, Object> maps = doGet(out_trade_no, transaction_id, total_fee, refund_fee, req, resp);
		maps.put("return_code", maps.get("return_code"));
		if (maps.get("return_code") == "SUCCESS" ) {
			maps.put("result_code", maps.get("result_code"));
		}
		return maps;
		
		
	}

	protected Map<String, Object> doGet(String out_trade_no, String transaction_id, String total_fee, String refund_fee,
			HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("退款");
		Map<String, Object> map = new HashMap<String, Object>();
		RefundUtil refundUtil = new RefundUtil();
		// 微信支付单号,总金额,退款总金额(可以分期退款)
		Map<String, String> record = null;
		try {
			record = refundUtil.wechatRefund(out_trade_no, transaction_id, total_fee, refund_fee, request);
			
			wxpayRefundService.insert(record);
			
			map.put("return_code", record.get("return_code"));
			if (record.get("return_code") == "SUCCESS" ) {
				map.put("result_code", record.get("result_code"));
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return map;
		

	}

	private  void balance(String openid, OrderVo order) {
		String total_money = (order.getTotal_money().multiply(new BigDecimal(100))).toString();
		BigDecimal bd100 = new BigDecimal(100);
		// 根据OpenId查询用户信息
		User user = userService.findByOpenId(openid);
		BigDecimal userMoney = user.getGeneralize_money();
		
		// 把支付金额（单位分）转化为 （单位元）
		BigDecimal bd = new BigDecimal(total_money).divide(bd100);
		
		// 判断用户钱包余额是否大于支付金额
		if (bd.compareTo(userMoney) == 1) {
			// 钱包金额小于支付金额
			// 修改用户钱包余额
			user.setGeneralize_money(new BigDecimal(0.00));
			userMapper.updateByPrimaryKey(user);
			
		} else {
			// 钱包金额大于支付金额
			// 修改用户钱包余额 钱包金额减去支付金额
			user.setGeneralize_money(userMoney.subtract(bd));
			userMapper.updateByPrimaryKey(user);
			
			
		}
	}
}
