package com.yqwl.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Order;
import com.yqwl.service.OrderService;
import com.yqwl.vo.OrderVo;

@Controller
@RequestMapping("order")
@Scope("prototype")
public class OrderController {
	@Resource
	private OrderService orderService;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)

	public String list(HttpSession session) {
		Administrator admin = (Administrator) session.getAttribute("login_admin");
		if (admin != null) {
			return "LiquorAdmin/view/orderAdmin/orderAdmin";
		} else {
			return "redirect:/login/toLogin.action";
		}
	}

	// 订单配置
	@RequestMapping(value = "system", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String system() {
		return "LiquorAdmin/view/orderAdmin/orderSystem";
	}

	// 订单详情
	@RequestMapping(value = "details", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String details() {
		return "LiquorAdmin/view/orderAdmin/orderInfo";
	}

	// 分配订单
	@RequestMapping(value = "allot", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String allot() {
		return "LiquorAdmin/view/orderAdmin/orderAllot";
	}

	/**
	 * 
	 * @Title: listAll
	 * @description: 查询所有订单加分页 模糊查询
	 *
	 * @param page
	 * @param limit
	 * @param id
	 * @param phone
	 * @param session
	 * @return
	 * @return String
	 * @author HanMeng
	 * @createDate 2019年2月21日-上午10:55:38
	 */

	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(String hm, Integer page, Integer limit, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			//Administrator admin = (Administrator) session.getAttribute("login_admin");
			//if (admin != null) {
				Map<String, Object> map = orderService.listAll(hm, page, limit);
			System.out.println(map);
				if (map.get("list") != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, map);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			//} //else {
			//	code = 2;
			//	msg = "未登录";
			//	return FastJsonUtil.getResponseJson(code, msg, null);
		//	}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: getByid
	 * @description 根据ID查询订单详情
	 * @param id
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年2月21日 下午2:43:05
	 */
	@ResponseBody
	@RequestMapping(value = "getByid", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getByid(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				OrderVo orderVo = orderService.getByid(id);
				if (orderVo != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, orderVo);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: getByStatus
	 * @description 根据状态分页查询订单
	 * @param page
	 * @param limit
	 * @param status
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年2月21日 下午2:43:17
	 */
	@ResponseBody
	@RequestMapping(value = "getByStatus", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getByStatus(Integer page, Integer limit, Integer status, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> map = orderService.getByStatus(status, page, limit);
				if (map.get("list") != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, map);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: getByCondition
	 * @description 根据条件分页查询订单
	 * @param page
	 * @param limit
	 * @param status
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年2月21日 下午2:43:17
	 */
	@ResponseBody
	@RequestMapping(value = "getByCondition", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String getByCondition(Integer page, Integer limit,
			@RequestParam(value = "subbranch_id", required = false) Long subbranch_id,
			@RequestParam(value = "deliveryman_name", required = false) String deliveryman_name,
			@RequestParam(value = "startTime", required = false) Long startTime,
			@RequestParam(value = "endTime", required = false) Long endTime, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> map = orderService.getByCondition(subbranch_id, deliveryman_name, startTime,
						endTime, page, limit);
				if (map.get("list") != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, map);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: updateByDeliveryman
	 * @description 分配订单转单
	 * @param Order
	 * @param session
	 * @return String
	 * @author likai
	 * @createDate 2019年2月21日 下午2:43:36
	 */
	@ResponseBody
	@RequestMapping(value = "updateByDeliveryman", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String updateByDeliveryman(Order Order, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = orderService.updateByDeliveryman(Order);
				if (i != 0) {
					code = 0;
					msg = "分配成功";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}
				code = 1;
				msg = "分配失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: findOrder
	 * @description: 查询订单状态（0 未支付 1、未配送，2、配送中，3、已送达，4、退货）
	 *
	 * @param session
	 * @param userId
	 * @param status
	 * @param limit
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年6月20日-上午11:20:18
	 */
	@ResponseBody
	@RequestMapping(value = "findOrder", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findOrder(HttpSession session, Integer userId, Integer status, Integer limit) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<OrderVo> order = orderService.findOrder(userId, status, limit);

				if (order != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, order);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
/**
 * 
 * @Title: orderCount
 * @description: 查询新订单数量（待接单）
 *
 * @param session
 * @return    
 * @return String   
 *
 * @author HanMeng
 * @createDate 2019年6月20日-上午11:19:53
 */
	@ResponseBody
	@RequestMapping(value = "orderCount", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String orderCount(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Integer amount = orderService.orderCount();
				if (amount != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, amount);
				} else {
					code = 1;
					msg = "查询失败";
					return FastJsonUtil.getResponseJson(code, msg, null);
				}

			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
	/**
	 * 
	 * @Title: deleteOrderPC
	 * @description: pc刷新的时候删除订单
	 * order/deleteOrderPC.action
	 *
	 * @param session
	 * @return    
	 * @return String   
	 *
	 * @author HanMeng
	 * @createDate 2019年6月27日-下午2:38:58
	 */
	@ResponseBody
	@RequestMapping(value = "deleteOrderPC", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String deleteOrderPC(HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				List<Order> order = orderService.deleteOrderPC();
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, order);				
				
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}
}
