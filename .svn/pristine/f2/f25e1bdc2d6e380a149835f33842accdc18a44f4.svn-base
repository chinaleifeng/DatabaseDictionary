package com.yqwl.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yqwl.common.utils.DateUtil;
import com.yqwl.common.utils.DesUtil;
import com.yqwl.common.utils.EntCoordSyncJob;
import com.yqwl.common.utils.LocationUtil;
import com.yqwl.common.utils.MD5Encrypt;
import com.yqwl.dao.DeliveryAddressMapper;
import com.yqwl.dao.DeliverymanMapper;
import com.yqwl.dao.LoginLogMapper;
import com.yqwl.dao.OrderMapper;
import com.yqwl.dao.SubbranchMapper;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.DeliveryAddress;
import com.yqwl.pojo.Deliveryman;
import com.yqwl.pojo.LoginLog;
import com.yqwl.pojo.Order;
import com.yqwl.pojo.Subbranch;
import com.yqwl.service.SubbranchService;
import com.yqwl.vo.LogVo;
import com.yqwl.vo.Turnover;

@Service("subbranchService")
@Transactional(rollbackFor = { Exception.class })
public class SubbranchServiceImpl implements SubbranchService {
	@Resource
	private SubbranchMapper subbranchMapper;
	@Resource
	private OrderMapper orderMapper;
	@Resource
	private DeliveryAddressMapper deliveryAddressMapper;
	@Resource
	private LoginLogMapper loginLogMapper;
	@Resource
	private DeliverymanMapper deliverymanMapper;

	/**
	 * 
	 * @Title: insert
	 * @description: 添加分店
	 *
	 * @param record
	 * @param admin
	 * @return
	 * @throws Exception
	 * @return int
	 *
	 * @author HanMeng
	 * @createDate 2019年1月3日-上午10:03:25
	 */
	@Override

	public int insert(Subbranch subbranch, Administrator admin) throws Exception {
		// 获取经纬度
		String coordinates = EntCoordSyncJob.getCoordinate(subbranch.getStore_location());
		//双层加密
		String DESPwd = DesUtil.encode("hanmeng1", subbranch.getPassword());
		String MD5Pwd = MD5Encrypt.MD5Encode(DESPwd);
		subbranch.setPassword(MD5Pwd);
		subbranch.setCoordinate(coordinates);
		subbranch.setCreation_time(new Date());
		return subbranchMapper.insert(subbranch);
	}

	/**
	 * 
	 * @Title: update
	 * @description: 修改分店信息
	 *
	 * @param subbranch
	 * @return
	 * @throws Exception
	 * @return int
	 *
	 * @author HanMeng
	 * @createDate 2019年1月3日-上午11:48:40
	 */
	@Override
	public int update(Subbranch subbranch) throws Exception {
		String DESPwd = DesUtil.encode("hanmeng1", subbranch.getPassword());
		String MD5Pwd = MD5Encrypt.MD5Encode(DESPwd);
		subbranch.setPassword(MD5Pwd);
		return subbranchMapper.updateByPrimaryKeySelective(subbranch);
	}

	/**
	 * 
	 * @Title: listAll
	 * @description: 分页加模糊查询（Name==null时查询全部）
	 *
	 * @param page
	 *            页数
	 * @param limit
	 *            条数
	 * @param name
	 *            分店名称
	 * @return
	 * @throws Exception
	 * @return Map<String,Object>
	 *
	 * @author HanMeng
	 * @createDate 2019年1月3日-下午2:59:14
	 */
	@Override
	public Map<String, Object> listAll(Integer page, Integer limit, String name) throws Exception {
		int resultCount = subbranchMapper.listAllCount(name);
		int beginPageIndex = ((page - 1) * limit);
		List<Subbranch> result = subbranchMapper.listAll(beginPageIndex, limit, name);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;
	}

	@Override
	public Subbranch selectByPrimaryKey(Long id) throws Exception {

		return subbranchMapper.selectByPrimaryKey(id);
	}

	@Override
	public Subbranch login(String account, String password) throws Exception {
		
		Subbranch subbranch = verify(account, password);
		return subbranch;

	}

	private Subbranch verify(String account, String password) {
		String DESPwd = DesUtil.encode("hanmeng1", password);
		String MD5Pwd = MD5Encrypt.MD5Encode(DESPwd);

		return subbranchMapper.selectByAccountAndPassword(account, MD5Pwd);
	}

	public List<Subbranch> findAlls() {
		return subbranchMapper.findAlls();
	}

	@Override
	public Long StepThree(Long order_id) throws Exception {
		Order order = orderMapper.selectByPrimaryKey(order_id);
		DeliveryAddress deliveryAddress = deliveryAddressMapper.selectByPrimaryKey(order.getAddress_id());
		// 查询出所有营业的分店
		List<Subbranch> subbranchs = subbranchMapper.listByStatus(0);
		// 收获的地址与分店的距离
		Double[] record = new Double[subbranchs.size()];
		// 所有店铺ID
		Long[] recordId = new Long[subbranchs.size()];
		int i = 0;
		// 循环遍历出所有店铺ID 和 收获的地址与分店的距离
		for (Subbranch subbranch : subbranchs) {
			recordId[i] = subbranch.getId();
			record[i] = (LocationUtil.GetPointDistance(subbranch.getCoordinate(),
					deliveryAddress.getLatitude().doubleValue(), deliveryAddress.getLongitude().doubleValue()));
			i++;
		}
		Long subbranch_id = recordId[0];
		Double min = record[0];
		// 循环比较出收货地址与哪个分店距离最近
		for (int j = 0; j < record.length; j++) {
			if (record[j] < min) {
				min = record[j];
				subbranch_id = recordId[j];
			}
		}
		order.setSubbranch_id(subbranch_id);
		orderMapper.updateByPrimaryKeySelective(order);
		return subbranch_id;
	}

	@Override
	public Map<String, Object> listLog(Integer page, Integer limit) throws Exception {
		List<LogVo> list = new LinkedList<>();
		int resultCount = loginLogMapper.LoginLogCount();
		int beginPageIndex = ((page - 1) * limit);
		List<LoginLog> result = loginLogMapper.LoginLog(beginPageIndex, limit);
		for (LoginLog loginLog : result) {
			LogVo logVo = new LogVo();
			BeanUtils.copyProperties(loginLog, logVo);
			Deliveryman deliveryman = deliverymanMapper.selectByPrimaryKey(loginLog.getDeliveryman_id());
			logVo.setDeliveryman(deliveryman);
			list.add(logVo);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", list);
		return map;
	}

	@Override
	public Map<String, Object> turnover(Integer page, Integer limit) throws Exception {
		Date date = new Date();
		int resultCount = subbranchMapper.turnoverCount(DateUtil.getYearMonth(date));
		int beginPageIndex = ((page - 1) * limit);
		List<Turnover> result = subbranchMapper.turnover(DateUtil.getYearMonth(date), beginPageIndex, limit);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", resultCount);
		map.put("list", result);
		return map;

	}
}
