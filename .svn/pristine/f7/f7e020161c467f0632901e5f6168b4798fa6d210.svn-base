package com.yqwl.controller;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yqwl.common.utils.Constants;
import com.yqwl.common.utils.FastJsonUtil;
import com.yqwl.pojo.Administrator;
import com.yqwl.pojo.Carousel;
import com.yqwl.pojo.Discounts;
import com.yqwl.service.CarouselService;

/**
 * 
 *
 * @ClassName: CarouselController
 *
 * @description 轮播图管理
 *
 * @author HanMeng
 * @createDate 2019年4月28日-下午1:47:32
 */
@Controller
@RequestMapping("carousel")
@Scope("prototype")
public class CarouselController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Resource
	private CarouselService carouselService;

	@RequestMapping(value = "list", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String list() {
		return "LiquorAdmin/view/photoAdmin/photoList";
	}

	@RequestMapping(value = "add", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String add() {
		return "LiquorAdmin/view/photoAdmin/photoAdd";
	}

	@RequestMapping(value = "info", method = RequestMethod.GET, produces = Constants.HTML_PRODUCE_TYPE)
	public String info() {
		return "LiquorAdmin/view/photoAdmin/photoInfo";
	}

	/**
	 * 
	 * @Title: insert
	 * @description: 新增轮播图
	 *
	 * @param carousel
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午1:50:31
	 */
	@ResponseBody
	@RequestMapping(value = "insert", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String insert(Carousel carousel, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = carouselService.insert(carousel);
				if (i != 0) {
					code = 0;
					msg = "新增成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: update
	 * @description: 修改轮播图
	 *
	 * @param carousel
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午1:52:57
	 */
	@ResponseBody
	@RequestMapping(value = "update", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String update(Carousel carousel, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = carouselService.update(carousel);
				if (i != 0) {
					code = 0;
					msg = "修改成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "新增失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: findById
	 * @description: 根据主键搜索轮播图（pc+小程序）
	 *
	 * @param id
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午1:54:40
	 */
	@ResponseBody
	@RequestMapping(value = "findById", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String findById(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {

			Carousel carousel = carouselService.findById(id);
			if (carousel != null) {
				code = 0;
				msg = "查询成功";
				return FastJsonUtil.getResponseJson(code, msg, carousel);
			}
			code = 1;
			msg = "查询失败";
			return FastJsonUtil.getResponseJson(code, msg, null);

		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: delete
	 * @description: 根据主键删除
	 *
	 * @param id
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午1:57:15
	 */
	@ResponseBody
	@RequestMapping(value = "delete", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String delete(Long id, HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				int i = carouselService.delete(id);
				if (i != 0) {
					code = 0;
					msg = "删除成功";
					return FastJsonUtil.getResponseJson(code, msg, i);
				}
				code = 1;
				msg = "删除失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

	/**
	 * 
	 * @Title: listAll
	 * @description: 查询所有轮播图
	 *
	 * @param page
	 * @param limit
	 * @param session
	 * @return
	 * @return String
	 *
	 * @author HanMeng
	 * @createDate 2019年4月28日-下午2:50:54
	 */
	@ResponseBody
	@RequestMapping(value = "listAll", method = RequestMethod.POST, produces = Constants.HTML_PRODUCE_TYPE)
	public String listAll(@RequestParam(value = "page", required = true) Integer page, Integer limit,
			HttpSession session) {
		int code = 0;
		String msg = null;
		try {
			Administrator admin = (Administrator) session.getAttribute("login_admin");
			if (admin != null) {
				Map<String, Object> adminList = carouselService.listAll(page, limit);
				if (adminList != null) {
					code = 0;
					msg = "查询成功";
					return FastJsonUtil.getResponseJson(code, msg, adminList);
				}
				code = 1;
				msg = "查询失败";
				return FastJsonUtil.getResponseJson(code, msg, null);
			} else {
				code = 2;
				msg = "未登录";
				return FastJsonUtil.getResponseJson(code, msg, null);
			}
		} catch (Exception e) {
			code = -1;
			msg = "系统异常";
			logger.error(e.getMessage(), e);
			return FastJsonUtil.getResponseJson(code, msg, e);
		}
	}

}
