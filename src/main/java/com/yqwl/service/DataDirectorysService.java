package com.yqwl.service;

import java.util.List;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.yqwl.pojo.DataDirectory;



public interface DataDirectorysService {

	 public String generateDataDirectory(DataDirectory dataDirectory,Connection conn);
	 
	 public Map<String, List<String>> getTableColumnsInfo(DataDirectory dataDirectory,Connection conn);

	 List<String> getColumnsInfo(DataDirectory dataDirectory, String table_name,Connection conn) ;
}
