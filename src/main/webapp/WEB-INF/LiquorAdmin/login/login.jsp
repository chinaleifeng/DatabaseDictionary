<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>首页</title>
<!-- 绝对路径 -->
<link rel="icon" href="/images/login.jpg" type="image/x-icon" />
<script src="https://cdn.jsdelivr.net/npm/layui-src@2/dist/layui.min.js"></script>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/layui-src@2/dist/css/layui.min.css">
<script src="https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/layui-src@2/dist/css/layui.min.css">
<script src='https://cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/element-ui@2/lib/index.min.js'></script>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/element-ui@2/lib/theme-chalk/index.min.css">
<style>
.login-main {
	position: relative;
	display: flex;
	justify-content: center;
	align-items: center;
	width: 100%;
	height: 100vh;
	background: url("/images/login.jpg") no-repeat center;
}

.login-form {
	background: rgba(255, 255, 255, 0.3);
	border-radius: 10px;
	padding: 50px;
}

.login-elip {
	color: #ffffff;
	font-size: 35px;
	text-align: center;
	letter-spacing: 5px;
	font-weight: bold;
}

.login-input-inline {
	position: relative;
	display: flex;
	width: 100%;
	margin-top: 30px;
}

.login-input-inline .layui-icon {
	position: absolute;
	left: 6px;
	top: 6px;
	font-size: 24px;
	color: #2F4056;
}

.login-input {
	background: rgba(255, 255, 255, 0.5);
	border-radius: 3px;
	border: 1px solid #ffffff;
	padding-left: 40px;
}
</style>
</head>
<body>
	<div class="login-main" id="app">
		<form class="layui-form login-form" lay-filter="login-form"
			name="frmlogin">
			<header class="login-elip">一键生成数据库字典</header>
			<div class="layui-input-inline login-input-inline">
				<p style="font-weight: bold; width: 100px; line-height: 38px;">username</p>
				<input type="text" name="user" lay-verify="required"
					placeholder="请输入数据库账号" class="layui-input login-input"
					lay-verType="tips">
			</div>
			<div class="layui-input-inline login-input-inline">
				<p style="font-weight: bold; width: 100px; line-height: 38px;">password</p>
				<input type="password" name="password" lay-verify="required"
					placeholder="请输入数据库密码" maxlength="30"
					class="layui-input login-input" lay-verType="tips">
			</div>
			<div class="layui-input-inline login-input-inline">
				<p style="font-weight: bold; width: 100px; line-height: 38px;">hostname</p>
				</i><input type="text" name="hostname" lay-verify="required"
					placeholder="请输入数据库ip/域名" class="layui-input login-input"
					lay-verType="tips">
			</div>
			<div class="layui-input-inline login-input-inline">
				<p style="font-weight: bold; width: 100px; line-height: 38px;">port</p>
				</i><input type="text" name="port" lay-verify="required"
					placeholder="请输入数据库端口" class="layui-input login-input"
					lay-verType="tips">
			</div>
			<div class="layui-input-inline login-input-inline">
				<p style="font-weight: bold; width: 100px; line-height: 38px;">database</p>
				</i><input type="text" name="database" lay-verify="required"
					placeholder="请输入数据库schema" class="layui-input login-input"
					lay-verType="tips">
			</div>
			<!-- <div class="layui-input-inline login-input-inline">
				<i class="layui-icon layui-icon-username"></i><input type="text"
					name="downloadPath" lay-verify="required" placeholder="请输入存储路径"
					class="layui-input login-input" lay-verType="tips">
			</div> -->
			<div class="layui-input-inline login-input-inline">
				<button type="button" class="layui-btn layui-btn-fluid"
					id="loginBtn" lay-submit lay-filter="sub">点我呀</button>
			</div>
			<br></br>
			<div>注:1.本地数据库必须进行内网穿透</div>
			<div> 　 2.默认存储在D:/</div>
			<div> 　 3.当生成成功，文件在被打开再次生成时会失败</div>
			<br></br>
			<div>失败的可能原因：1.数据库名ip输入的是localhost或者127.0.0.1</div>
			
			<div>　  　  　  　  　  　     　2.其余参数错误</div>

		</form>
		<div
			style="position: fixed; bottom: 50px; width: 100%; text-align: center; left: 0; color: #fff;">2020-
			www.hanmeng.work All Rights Reserved 冀ICP备19037629</div>
		<!-- <div class="layui-footer">
  底部固定区域
  ©2018 喝小酒儿 All rights resered <span style="padding-left: 20px;">石家庄云阙网络科技有限公司提供技术支持</span>
</div> -->
	</div>
</body>
<script>
	layui.use([ 'form' ], function() { // 如果只加载一个模块，可以不填数组。如：layui.use('form')
		var form = layui.form;
		// 提交按钮
		form.on('submit(sub)', function(data) {
			window.location.href = "http://127.0.0.1:8056/login/login.action?user="+data.field.user+"&password="+data.field.password+"&hostname="+data.field.hostname+"&port="+data.field.port+"&database="+data.field.database;
			return false; // 阻止表单跳转。如果需要表单跳转，去掉这段即可。
		})
	})
	$(document).keypress(function(e) {
		if ((e.keyCode || e.which) === 13) {
			$("#loginBtn").click(); // login_btn登录按钮的id
		}
	});
	
	const
	mixin = {}
</script>
</html>
<%@ include file="../view/layout/vue.jsp"%>
