<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="layui-footer">
  <!-- 底部固定区域 -->
  ©2018 喝小酒儿 All rights resered <span style="padding-left: 20px;">石家庄云阙网络科技有限公司提供技术支持</span>
</div>
</div>
</div>
<script>
  //JavaScript代码区域
  layui.use(['form', 'element'], function () {
    var form = layui.form, element = layui.element;
    // 设定表单验证正则表达式
    form.verify({
      password: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],
      price: [/^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/, '请输入正确的价格'],
      respon: [/^-?[0-9]\d*$/, '请输入正确的库存'], // 正整数
    });
  });

  // 垂直导航激活
  function navActive(index) {
    $($('.layui-side .layui-nav-item')[index]).addClass('layui-nav-itemed');
  }

  // 二级导航激活
  function secondNavActive(path, index) {
    $($(path)[index]).addClass('layui-this');
  }

  // 返回
  function back() {
    window.history.back();
    return false;
  }

  // 获取地址栏参数，name:参数名称
  function getHrefParam(key) {
    var s = window.location.href;
    var reg = new RegExp(key + "=\\w+");
    var rs = reg.exec(s);
    if (rs === null || rs === undefined) {
      return "";
    } else {
      return rs[0].split("=")[1];
    }
  }

  // 格式化Date日期时间数据(yyyy-MM-dd hh:mm:ss)
  function timeStamp2String(time) {
    var datetime = new Date(time);
    var year = datetime.getFullYear();
    var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
    var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
    var h = datetime.getHours();
    var m = datetime.getMinutes();
    var se = datetime.getSeconds();
    return year + "-" + month + "-" + date + " " + h + ":" + m + ":" + se;
  }

  // 表单验证提示方式
  $(document).ready(function () {
    $('input').attr('lay-vertype', 'alert');
    $('select').attr('lay-vertype', 'alert');
    $('textarea').attr('lay-vertype', 'alert');
  })

  // 弹窗加跳页面
  function layerMsgPath(msg, path) {
    layer.alert(msg, {
      skin: 'layui-layer-molv' // 样式类名
      , closeBtn: 0
    }, function () {
      window.location.href = path
    });
  }

  // 弹窗不跳页面
  function layerClose(msg) {
    layer.alert(msg, {
      skin: 'layui-layer-molv' // 样式类名
      , closeBtn: 0
    }, function () {
      layer.closeAll();
    });
  }

  function imgLayer(formImgUrl) {
    if (formImgUrl.length === 0) {
      $('.imgList').append($($('.imgBox')[0]).clone());
      $($('.imgBox img')[0]).attr('src', '../images/nodata.png');
    } else {
      for (var i = 0; i < formImgUrl.length; i++) {
        if (formImgUrl[i].type == 3) {
          $('.imgList-1').append(
            '<div class="imgBox"><i class="layui-icon layui-icon-delete" style="font-size: 30px" onclick="deleteImg(' + formImgUrl[i].id + ')"></i><img src="'
            + formImgUrl[i].url + '"/></div>');
        } else if (formImgUrl[i].type == 1) {
          $('.imgList-2').append(
            '<div class="imgBox"><i class="layui-icon layui-icon-delete" style="font-size: 30px" onclick="deleteImg(' + formImgUrl[i].id + ')"></i><img src="'
            + formImgUrl[i].url + '"/></div>');
        } else if (formImgUrl[i].type == 2) {
          $('.imgList-3').append(
            '<div class="imgBox"><i class="layui-icon layui-icon-delete" style="font-size: 30px" onclick="deleteImg(' + formImgUrl[i].id + ')"></i><img src="'
            + formImgUrl[i].url + '"/></div>');
        }
      }
    }
  }

  function deleteImg(id) {
    layer.confirm('是否删除?', {
      title: '提示',
      skin: 'layui-layer-molv',
    }, function (index) {
      // do something
      console.log(id)
      $.ajax({
        url: '/goods/deleteGoodsImg.action',
        type: 'post',
        data: {
          id: id
        },
        dataType: 'json',
        success: function (data) {
          console.log(data)
          if (data.code === 0) {
            layerClose(data.msg)
            window.location.reload()
          }
        }
      })
      layer.msg(index);
    });
  }

  function logout() {
    $.ajax({
      url: '/login/logout.action',
      type: 'post',
      dataType: 'json',
      success: function (data) {
        if (data.code === 0) {
          window.location.href = '/login/toLogin.action'
        } else {
          layerClose(data.msg)
        }
      }
    })
  }
</script>
</body>
</html>
