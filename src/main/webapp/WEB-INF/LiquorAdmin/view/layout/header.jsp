<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="/images/login.jpg" type="image/x-icon" />
<script src="https://cdn.jsdelivr.net/npm/layui-src@2/dist/layui.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/layui-src@2/dist/css/layui.min.css">
<script src="https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
<script src='https://cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js'></script>
<script src='https://cdn.jsdelivr.net/npm/element-ui@2/lib/index.min.js'></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/element-ui@2/lib/theme-chalk/index.min.css">
<script type="text/javascript">
	if (navigator.appName == 'Microsoft Internet Explorer') {
		if (navigator.userAgent.indexOf("MSIE 5.0") > 0 || navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0) {
			alert('您使用的 IE 浏览器版本过低, 推荐使用 Chrome 浏览器或 IE8 及以上版本浏览器.');
		}
	}
</script>
<style>
html, body {
	min-width: 500px;
}
.layui-body{
	overflow:scroll!important;
}
.layui-body::-webkit-scrollbar {
    display: none;
}
/* 导航 */
.layui-layout-right .layui-nav-bar {
	opacity: 0 !important;
}

.layui-nav-itemed>a {
	border-top: 1px solid #72808D;
	border-bottom: 1px solid #192431;
}

.layui-disabled, .layui-disabled:hover {
	color: #757575 !important;
}

.user {
	padding-left: 10px;
}

.layui-bg-theme {
	background: #393D49 !important;
}

.userName {
	line-height: 120px;
	color: #fff;
}

.menu-ioc {
	margin-right: 10px;
	font-size: 18px;
}

.circular {
	margin-right: 15px;
	height: 42px;
}
/* 搜索 */
.search-input input {
	height: 40px;
	border-radius: 20px;
	outline: none;
	border: 1px solid #ccc;
	padding: 0 20px 0 40px;
	font-size: 15px;
}

.layui-icon-search {
	position: absolute;
	left: 16px;
	top: 14px;
}

#realEstateMortgage {
	margin-top: 10px;
}

.layui-table thead tr {
	background-color: #405467;
	color: #fff;
}

.layui-table tr {
	color: #73879c;
}

/*面包屑*/
.breadcrumb {
	width: 200px;
	height: 21px;
}

.blue {
	color: #009688;
}

.gray {
	color: #c2c2c2;
}
/*表单*/
.layui-field-title {
	margin: 20px 0 30px;
	font-weight: bolder;
	color: #181818;
}
#fashion span{
	height:38px!important;
}

.form-main {
	padding: 50px;
	color: #757575;
	min-width: 666px;
}

.form-main input {
	color: #757575;
}

.layui-form-label {
	width: 120px;
}

.util {
	position: absolute;
	right: -20px;
	top: 9px;
}

.layui-input-inline {
	width: 250px !important;
}

.imgList img {
	width: 100%;
	height: 100%;
}

.imgList .imgBox {
	position: relative;
	width: 200px;
	height: 200px;
	float: left;
	margin: 4px;
}
.imgList .imgBox .layui-icon{
	position: absolute;
	right: 10px;
	top: 10px;
}
/* 按钮组 */
.btn-item {
	padding-top: 50px;
}

.btn-item button {
	width: 145px;
	height: 45px;
	border-radius: 5px;
}
/* 输入框小插件 */
.layui-input-inline .unit {
	position: absolute;
	left: 200px;
	top: 10px;
	width: 40px;
}
.conts{
    position: absolute;
    top: 135px;
    right: 125px;
    width: 700px;
    height: 500px;
}
</style>
</head>
<body class="layui-layout-body">
	<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
	<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	<div class="layui-layout layui-layout-admin" id="app">
		<div class="layui-header layui-bg-theme">
			<div class="layui-logo">
				<a style="color: rgba(255, 255, 255, .7);">喝小酒儿管理系统</a>
			</div>
			<ul class="layui-nav layui-layout-right">
			 <li class="layui-nav-item" lay-unselect><a href="/admin/msg.action"><i class="layui-icon layui-icon-notice" style="font-size: 30px;"></i>
</a></li>
				<li class="layui-nav-item" lay-unselect><a href="javascript:;"> <img src="/images/head.jpg" class="layui-nav-img">超级管理员
				</a>
					<dl class="layui-nav-child">
						<dd>
							<a href="/admin/list.action">修改密码</a>
						</dd>
						<dd>
							<a href="javascript:;" onclick="logout()">退出</a>
						</dd>
					</dl></li>
			</ul>
		</div>
		<div class="layui-side layui-bg-theme">
			<div class="layui-side-scroll">
				<div class="user">
					<img src="/images/head.jpg" class="layui-nav-img"> <span class="userName">欢迎您，超级管理员</span>
				</div>
				<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
				<ul class="layui-nav layui-nav-tree layui-bg-theme">
					<li class="layui-nav-item" id="branchAdmin"><a class="" href="javascript:;"><i class="layui-icon layui-icon-layouts menu-ioc"></i>店铺管理</a>
						<dl class="layui-nav-child">
							<dd>
								<a href="/subbranch/list.action"> <img src="/images/circular.png" class="circular">店铺管理
								</a>
							</dd>
							<dd>
								<a href="/admin/branchMoney.action"> <img src="/images/circular.png" class="circular">分店业绩
								</a>
							</dd>
							<dd>
								<a href="/deliveryman/allot.action"> <img src="/images/circular.png" class="circular">配送员管理
								</a>
							</dd>
							<dd>
								<a href="/subbranch/Distributor.action"> <img src="/images/circular.png" class="circular">总店销量
								</a>
							</dd>
						</dl></li>
						<li class="layui-nav-item" id="waresAdmin"><a href="javascript:;"><i class="layui-icon layui-icon-templeate-1 menu-ioc"></i>商品管理</a>
							<dl class="layui-nav-child">
								<dd>
									<a href="/goods/list.action"><img src="/images/circular.png" class="circular">商品管理</a>
								</dd>
								<dd>
									<a href="/fashion/list.action"><img src="/images/circular.png" class="circular">模块管理</a>
								</dd>
								<dd>
									<a href="/discounts/list.action"><img src="/images/circular.png" class="circular">优惠券管理</a>
								</dd>
							</dl>
						</li>
						<li class="layui-nav-item" id="order"><a href="javascript:;"><i class="layui-icon layui-icon-layouts menu-ioc"></i>订单管理</a>
							<dl class="layui-nav-child">
								<dd>
									<a href="/order/list.action"><img src="/images/circular.png" class="circular">订单管理</a>
								</dd>
								<dd>
									<a href="/order/system.action"><img src="/images/circular.png" class="circular">订单设置</a>
								</dd>
							</dl>
						</li>
						<li class="layui-nav-item" id="user"><a href="javascript:;"><i class="layui-icon layui-icon-templeate-1 menu-ioc"></i>客户管理</a>
							<dl class="layui-nav-child">
								<dd>
									<a href="/user/list.action"><img src="/images/circular.png" class="circular">客户管理</a>
								</dd>
							</dl>
						</li>
						<li class="layui-nav-item" id="photoAdmin"><a href="javascript:;"><i class="layui-icon layui-icon-layouts menu-ioc"></i>图片管理</a>
							<dl class="layui-nav-child">
								<dd>
									<a href="/carousel/list.action"><img src="/images/circular.png" class="circular">轮播图管理</a>
								</dd>
								<dd>
									<a href="/twoCodeIntroduce/list.action"><img src="/images/circular.png" class="circular">二维码管理</a>
								</dd>
							</dl>
						</li>
				</ul>
			</div>
		</div>
		<div class="layui-body">
			<!-- 内容主体区域 -->
