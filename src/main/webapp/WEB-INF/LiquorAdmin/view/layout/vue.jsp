<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>
  let vm = new Vue({
    el: '#app',
    mixins: [mixin],
    data: {
      username: window.sessionStorage.getItem('username'),
      menu: JSON.parse(window.sessionStorage.getItem('menu'))
    },
    methods: {
      timeFormat(time) {
        var datetime = new Date(time);
        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
        var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
        var h = datetime.getHours();
        var m = datetime.getMinutes();
        var se = datetime.getSeconds();
        return year + "-" + month + "-" + date + " " + h + ":" + m + ":" + se;
      }
    }
  })
  Vue.use(ELEMENT)
</script>
